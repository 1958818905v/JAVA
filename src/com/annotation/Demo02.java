package com.annotation;

import java.util.LinkedList;
import java.util.List;

/**
 * @Description: TODO(内置注解)
 * @Author lenovo
 * @Date 2021/8/11 9:01
 */
@SuppressWarnings("all")
public class Demo02 extends Object {
    /**
     * @Override：定义在java.lang.Override中，此注解只适用于修辞方法，表示一个方法声明打算重写超类中的另一个方法声明
     * @Deprecated: 定义在java.lang.Deprecated中，此注解用于修辞方法，属性，类，表示已废弃
     * @SuppressWarnings: 定义在java.lang.SuppressWarnings中，用来抑制编译时的警告信息
     * ·这个注解需要加一个参数  @SuppressWarnings（“all”）
     */
    @Override
    public String toString() {
        return super.toString();
    }
    @Deprecated
    public static void Demo03(){
        System.out.println(1);
    }


    public static void test(){
        List list = new LinkedList();
    }

    public static void main(String[] args) {
        Demo03();
    }
}
