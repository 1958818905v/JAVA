package com.annotation;

/**
 * @Description: TODO(注解)
 * @Author lenovo
 * @Date 2021/8/11 8:54
 */
public class Demo01 extends Object {
    /**
     * 框架底层实现都是注解
     * JDK5.0引入
     * 作用：
     *      1.不是程序本身，可以对程序做出解释
     *      2.可以被其他程序（编译器读取）
     * 格式：
     *      注解是以“@注解名”在代码中存在的，还可以添加一些参数值，例如：@SuppressWarnings（Value=“unchecked”）
     *          `@Override：重写
     * 在哪里使用
     *      可以附加在package，class。method，field等上面，相当于添加额外的辅助信息
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
