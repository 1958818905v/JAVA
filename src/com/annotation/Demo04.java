package com.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description: TODO(自定义注解)
 * @Author lenovo
 * @Date 2021/8/11 9:38
 */
public class Demo04 {
    /**
     * 关键字：@interface 自动继承java.lang.annotation.Annotation
     *  ·@interface用来声明一个注解，格式：public @ interface 注解名{定义内容}
     *  ·其中的每个方法实际上时声明了一个参数配置
     *  ·方法的名字就是参数的名称
     *  ·返回值类型就是参数的类型（返回值只能是基本类型，Class、String、enum）
     *  ·可以通过default来声明参数的默认值
     *  ·如果只有一个参数成员，一般参数名为value
     *  ·注解元素必须要有值，我们定义注解元素是，经常使用空字符串，0作为默认值
     */
    @MyAnnotation2()
    public void test(){}
}
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation2{
    // 参数类型+参数名（）；
    // 只有一个参数用value赋值
    String name() default "";
    int age() default 0;
    int id() default -1;

    String[] schools() default {"清华","北大"};
}

