package com.networkProgramming;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

/**
 * @Description: TODO(IP)
 * @Author lenovo
 * @Date 2021/7/30 11:28
 */
public class Demo02 {
    /**
     * ip地址：InetAddress
     *  ·唯一定位一台网络上的计算机
     *  ·127.0.0.1（localhost）
     *  ·ip地址的分类：
     *      1.ip地址的分类
     *          1.1 ipv4：四个字节组成，0-255 大概42亿个 30亿在北美，亚洲4亿 2011年用尽
     *          1.2 ipv6：128位。8个无符号整数！
     *      2.公网-私网
     *          ·公网（互联网）：ABCD类地址
     *          ·私网（局域网）：给组织内部使用
     *      3.域名：记忆IP问题！
     */
    public static void main(String[] args) {
        try {
            InetAddress[] inetAddresses = InetAddress.getAllByName("www.baidu.com");
            System.out.println(Arrays.toString(inetAddresses));


        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
