package com.networkProgramming;

/**
 * @Description: TODO(通信协议)
 * @Author lenovo
 * @Date 2021/7/30 14:47
 */
public class Demo04 {
    /**
     * 协议：就是一种约定
     * 重要的协议：
     *  1.TCP：用户传输协议
     *  2.UDP：用户数据报协议
     * 出名的协议：
     *  1.TCP：
     *  2.IP：网络互联协议
     * TCP UDP对比：
     *  1.TCP：打电话
     *      1.连接、稳定
     *      2.三次握手，四次挥手
     *      3.客户端、服务端
     *      4.传输完成、释放连接、效率低
     *  2.UDP：发短信
     *      1.不连接、不稳定
     *      2.客户端、服务端：没有明确的界限
     *      3.不管有没有准备好，都可以发送你
     *      4.导弹
     *      5.DDOS：洪水攻击！饱和攻击
     */
}
