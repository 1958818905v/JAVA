package com.networkProgramming;

import java.net.SocketException;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/8/10 10:40
 */
public class TalkStudent {
    public static void main(String[] args) {
        try {
            new Thread(new TalkSend(7777, "127.0.0.1", 9999)).start();
            new Thread(new TalkReceive(8888, "老师")).start();
        } catch (SocketException e) {
            e.printStackTrace();
        }

    }
}
