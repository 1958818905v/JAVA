package com.networkProgramming;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/8/2 8:43
 */
public class TCPDemo05 {
    public static void main(String[] args) throws IOException {
//        1.创建服务
        ServerSocket serverSocket = new ServerSocket(9000);
//        2.监听客户端连接
        Socket socket = serverSocket.accept();
//        3.获取输入流
        InputStream is = socket.getInputStream();
//        4.文件输出
        FileOutputStream fos = new FileOutputStream(new File("receive.jpg"));
        byte[] bytes = new byte[1024];
        int len;
        while ((len = is.read(bytes)) != -1) {
            fos.write(bytes, 0, len);
        }

//        通知客户端接收完毕
        OutputStream ops = socket.getOutputStream();
        fos.write("接收完毕！".getBytes());

        fos.close();
        is.close();
        socket.close();
        serverSocket.close();


    }
}
