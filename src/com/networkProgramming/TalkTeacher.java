package com.networkProgramming;

import java.net.SocketException;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/8/10 10:40
 */
public class TalkTeacher {
    public static void main(String[] args) {
        try {
            new Thread(new TalkSend(5555, "127.0.0.1", 8888)).start();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        new Thread(new TalkReceive(9999, "学生")).start();
    }
}

