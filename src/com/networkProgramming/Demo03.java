package com.networkProgramming;

import java.net.InetSocketAddress;

/**
 * @Description: TODO(端口)
 * @Author lenovo
 * @Date 2021/7/30 11:51
 */
public class Demo03 {
    /**
     * 端口表示计算机上的一个程序的进程
     *  ·不同的进程有不同的端口号，不能冲突（用来区分软件）
     *  ·被规定0-65535
     *  ·TCP端口和UDP端口：协议不冲突
     *  端口分类：
     *      1. 0-1023（公有端口）
     *          ·HTTP：80
     *          ·HTTPS：443
     *          ·FTP：21
     *          ·Telent：23
     *      2.程序注册端口号（1204-49151），分配用户和程序
     *          ·Tomcat：8080
     *          ·Mysql：3306
     *          ·Oracle：1521
     *      3.动态、私有（49152-65535）
     *          ·netstat -ano
     */

    public static void main(String[] args) {
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 8080);
        System.out.println(inetSocketAddress);
        System.out.println(inetSocketAddress.getAddress());
        System.out.println(inetSocketAddress.getHostName());
    }



}
