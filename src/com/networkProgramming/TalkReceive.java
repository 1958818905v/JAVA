package com.networkProgramming;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/8/10 10:32
 */
public class TalkReceive implements Runnable {
    DatagramSocket socket = null;
    private int Port;
    private String msgForm;

    public TalkReceive(int Port, String msgForm) {
        this.Port = Port;
        this.msgForm = msgForm;
        try {
            socket = new DatagramSocket(Port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                byte[] container = new byte[1024];
                DatagramPacket packet = new DatagramPacket(container, 0, container.length);
                socket.receive(packet);

                byte[] data = packet.getData();
                String receiveDate = new String(data, 0, data.length);

                System.out.println(msgForm + "：" + receiveDate);
                if (receiveDate.equals("bye")) {
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            socket.close();

        }
    }
}
