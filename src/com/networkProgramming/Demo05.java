package com.networkProgramming;

import java.io.IOException;
import java.net.*;

/**
 * @Description: TODO(UDP)
 * @Author lenovo
 * @Date 2021/8/9 10:24
 */
public class Demo05 {
    /**
     * 不用连接服务器需要地址
     * <p>
     * DatagramPacket
     * DatagramSocket
     */
    public static void main(String[] args) throws IOException {

        DatagramSocket socket = new DatagramSocket();

        String msg = "你好啊";
        InetAddress localhost = InetAddress.getByName("127.0.0.1");
        int port = 9090;
        DatagramPacket packet = new DatagramPacket(msg.getBytes(), 0, msg.getBytes().length, localhost, port);

        socket.send(packet);

        socket.close();

    }

}
