package com.networkProgramming;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/8/9 10:46
 */
public class Demo06 {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(9090);
        byte[] buffer = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);

        // 阻塞接收
        socket.receive(packet);
        System.out.println(packet.getAddress().getHostAddress());
        System.out.println(new String (packet.getData(),0, packet.getLength()));

        socket.close();
    }
}
