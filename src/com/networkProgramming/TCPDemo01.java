package com.networkProgramming;

import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @Description: TODO(客户端)
 * @Author lenovo
 * @Date 2021/7/30 15:01
 */
public class TCPDemo01 {
    public static void main(String[] args) {
        // 1.要知道服务器的地址,端口号
        try {
            InetAddress byName = InetAddress.getByName("127.0.0.1");
            int port = 9999;
        // 2.创建连接
            Socket socket = new Socket(byName, port);
        // 3.发消息IO流
            OutputStream os = socket.getOutputStream();
            os.write("你好,服务端!".getBytes());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
