package com.networkProgramming;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @Description: TODO(上传文件)
 * @Author lenovo
 * @Date 2021/7/30 15:24
 */
public class TCPDemo04 {
    public static void main(String[] args) throws IOException {
//        1. 创建Socket连接
        Socket socket = new Socket(InetAddress.getByName("127.0.0.1"), 9000);
//        2.创建输出流
        OutputStream os = socket.getOutputStream();
//        3.读取文件
        FileInputStream fis = new FileInputStream(new File("javase/src/com/qijiang.jpg"));
//        4.写出文件
        byte[] buffer = new byte[1024];
        int len;
        while ((len = fis.read(buffer)) != -1) {
            os.write(buffer, 0, len);
        }
        socket.shutdownOutput();

//        确定服务器接受完毕
        InputStream inputStream = socket.getInputStream();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buffer2 = new byte[1024];
        int len2;
        while ((len2=inputStream.read())!=-1){
            bos.write(buffer2,0,len2);
        }
        System.out.println(bos.toString());

        bos.close();
        inputStream.close();
        fis.close();
        os.close();
        socket.close();
    }

}
