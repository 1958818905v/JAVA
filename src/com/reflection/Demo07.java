package com.reflection;

import java.lang.reflect.Field;

/**
 * @Description: TODO(获取类运行时的结构)
 * @Author lenovo
 * @Date 2021/8/11 16:28
 */
public class Demo07 {
    /**
     *
     */
    public static void main(String[] args) throws ClassNotFoundException {
        Class c1 = Class.forName("com.reflection.User");

        System.out.println(c1.getName());
        System.out.println(c1.getSimpleName());

        Field[] fields = c1.getDeclaredFields();
        for (Field f : fields) {
            System.out.println(f);
        }



    }
}
