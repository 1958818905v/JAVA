package com.reflection;

import java.lang.annotation.*;
import java.lang.reflect.AnnotatedType;

/**
 * @Description: TODO(获取注解信息)
 * @Author lenovo
 * @Date 2021/8/11 17:17
 */
public class Demo10 {
    /**
     * getAnnotations
     * getAnnotation
     */
    public static void main(String[] args) throws ClassNotFoundException {
        Class c1 = Class.forName("com.reflection.Student08");
        AnnotatedType[] interfaces = c1.getAnnotatedInterfaces();
        for (AnnotatedType anInterface : interfaces) {
            System.out.println(anInterface);
        }

        Table table = (Table) c1.getAnnotation(Table.class);
        String value = table.value();
        System.out.println(value);


    }

}

@Table("db_student")
class Student08 {
    @Field(columnName = "db_id", type = "int", length = 10)
    private int id;
    @Field(columnName = "db_age", type = "int", length = 10)
    private int age;
    @Field(columnName = "db_name", type = "varchar", length = 10)
    private String name;

    @Override
    public String toString() {
        return "Student08{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student08(int id, int age, String name) {
        this.id = id;
        this.age = age;
        this.name = name;
    }

    public Student08() {
    }
}

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Table {
    String value();
}


@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@interface Field {
    String columnName();

    String type();

    int length();
}