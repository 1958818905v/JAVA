package com.reflection;

/**
 * @Description: TODO(类加载器)
 * @Author lenovo
 * @Date 2021/8/11 16:07
 */
public class Demo06 {
    /*
    类加载的作用：
        将class文件字节码内容加载到内存中，并将这些静态数据转换成方法区的运行时数据结构，然后在堆中生成一个代表这个类的java.lang.Class对象，作为方法去中类数据的访问入口
    类缓存：
        标准的JavaSE类加载器可以按要求查找类，但一旦某个类被加载到类加载器中，它将维持加载（缓存）一段时间。不过JVM垃圾回收机制可以回收这些Class对象

     分类：
           引导类加载器：用C++编写的，是JVM自带的类加载器，负责Java平台核心库，用来装载核心类库（rt.jar），该加载器无法直接获取
           扩展类加载器：负责jre、lib、ext目录下的jar包或指定目录下的jar包装入工作库中
           系统类加载器：负责java-classpath或——D java.class.path所指的目录下的类与jar包装入工作，是最常用的加载器
     */
    public static void main(String[] args) throws ClassNotFoundException {
        // 系统类加载器
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        System.out.println(systemClassLoader);
        // 扩展类加载器
        ClassLoader parent = systemClassLoader.getParent();
        System.out.println(parent);

        // 根加载器（c/C++）
        ClassLoader parent1 = parent.getParent();
        System.out.println(parent1);


        ClassLoader c1 = Class.forName("com.reflection.Demo06").getClassLoader();
        System.out.println(c1);

        /*
        双亲委派机制
            多重检测，保证安全
         */
    }
}

