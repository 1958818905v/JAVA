package com.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @Description: TODO(动态创建对象)
 * @Author lenovo
 * @Date 2021/8/11 16:43
 */
public class Demo08 {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        Class c1 = Class.forName("com.reflection.User");

        // 构造对象
        /*
        1.必须要有无参构造器
        2.类的构造器的访问权限需要足够
         */
       // User user = (User) c1.newInstance();
       // System.out.println(user);

        // 构造器创建对象
//        Constructor constructor = c1.getDeclaredConstructor(String.class, int.class, int.class);
//        User user2 =(User) constructor.newInstance("abc", 001, 18);
//        System.out.println(user2);

        // 反射调用方法
//        User user3 = (User) c1.newInstance();
//        Method setName = c1.getDeclaredMethod("setName", String.class);
//        // 对象 对象值
//        setName.invoke(user3,"abc");
//        System.out.println(user3.getName());

        // 反射操作属性
        User user4 = (User) c1.newInstance();
        Field name = c1.getDeclaredField("name");
        // 不能直接操作私有属性，需要关闭代码检测
        name.setAccessible(true);
        name.set(user4,"abc");
        System.out.println(user4.getName());
    }
}
