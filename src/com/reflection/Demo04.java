package com.reflection;

/**
 * @Description: TODO(那些类型可以有Class对象)
 * @Author lenovo
 * @Date 2021/8/11 11:09
 */
public class Demo04 {
    /**
     * class：外部类，成员（成员内部类，静态内部类），局部内部类，匿名内部类
     * interface：接口
     * []：数组
     * enum：枚举
     * annotation：注解@interface
     * primitive type：基本数据类型
     * void
     */
}
