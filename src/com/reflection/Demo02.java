package com.reflection;

/**
 * @Description: TODO(获取反射对象)
 * @Author lenovo
 * @Date 2021/8/11 10:36
 */
public class Demo02 {
    /**
     * 优点：可以实现动态创建对象和编译，体现出很大的灵活性
     * 缺点：对性能有影响
     */
    public static void main(String[] args) throws ClassNotFoundException {
        Class c1 = Class.forName("com.reflection.User");
        Class c2 = Class.forName("com.reflection.User");
        Class c3 = Class.forName("com.reflection.User");
        System.out.println(c1.hashCode());
        System.out.println(c2.hashCode());
        System.out.println(c3.hashCode());


    }

}

// 实体类pojo
class User {
    private String name;
    private int id;
    private int age;

    public User() {
    }

    public User(String name, int id, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}