package com.reflection;

/**
 * @Description: TODO(类加载内存分析)
 * @Author lenovo
 * @Date 2021/8/11 14:30
 */
public class Demo05 {
    /**
     * java内存：
     * 1.堆：·存放new的对象和数组
     * ·可以被所有的线程共享，不会存放别的对象引用
     * 2.栈：·存放基本变量类型（会包含这个基本类型的具体数值）
     * ·引用对象的变量（会存放这个引用在堆里面的具体地址）
     * 3.方法区：·可以被所有的线程共享
     * ·包含了所有的class和static变量
     */
    public static void main(String[] args) {
        a a1 = new a();
        System.out.println(a.m);
        /*
        1.加载到内存，产生一个类对应class对象
        2.链接，链接结束后m=0
        3.初始化
            <clinit>(){
                System.out.println（“初始化”）；
                m=300;
                m=100;
            }
         */
    }
}

class a {
    static {
        System.out.println("初始化");
    }

    static int m = 100;

    public a() {
        System.out.println("无参构造");
    }
}

/*
    类初始化
        类的主动引用（一定会发生类的初始化）
            1.当虚拟机启动，先初始化main方法所在的类
            2.new一个类的对象
            3.调用类的静态变量（除了final常量）和静态方法
            4.使用java.lang.reflect包的方法对类进行反射调用
            5.当初始化一个类，如果其父亲没有被初始化，则先会初始化它的父类
         类的被动引用（不会发生类的初始化）
            1.当访问一个静态域时，只有真正声明这份域的类才会被初始化。
            2.通过数组定义类引用，不会触发此类的初始化
            3.引用常量不会触发此类的初始化
 */