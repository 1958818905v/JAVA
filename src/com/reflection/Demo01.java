package com.reflection;

/**
 * @Description: TODO(反射)
 * @Author lenovo
 * @Date 2021/8/11 9:59
 */
public class Demo01 {
    /**
     * 动态语言：
     *  ·是一类在运行时可以改变其结构的语言：例如新的函数，对象，甚至代码可以被引进，已有的函数可以被删除或是其他结构上的变化
     *  ·主要动态语言：Object-C、C#、JavaScript、PHP、Python等
     * 静态语言：
     *  ·与动态语言相对应的，运行时结构不可变得语言就是静态语言。如java、C、C++
     *  ·java不是动态语言，但java可以称之为“准动态语言”。即Java有一定的动态性，我们可以利用反射机制获得类似鼎泰语言的特性。
     *
     *  ·Reflection（反射）是java被视为动态语言的关键，反射机制允许程序在执行期借助于Reflection API获取任何类的内部信息，并能直接操作任意对象的内部属性及方法
     *  Class c = Class.forName("java.lang.String")
     *  ·加载完类之后，在堆内训的方法区中就产生了一个Class类型的对象（一个类只有一个Class对象），这个对象就包含这个类的完整结构信息
     *  我们可以通过这个对象看到类的结构。这个对象就想一面镜子，透过这个镜子看到类的结构，所以我们称之为反射
     *
     */
}
