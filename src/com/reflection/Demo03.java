package com.reflection;

/**
 * @Description: TODO(得到Class类的几种方法)
 * @Author lenovo
 * @Date 2021/8/11 10:48
 */
public class Demo03 {
    /**
     * Class方法：
     * 1.ClassforName:返回指定类名name的Class对象
     * 2.newInstance：调用缺省构造函数，返回Class对象的一个实例
     * 3.getName；返回class对象所表示的试题（类、接口、数组类或void）的名称
     * 4.getSuperClass（）：返回当前Class对象的父类的class对象
     * 5.getinterfaces（）；获取当前class对象的接口
     * 6.getClassLoader（）：返回该类的类加载器
     * 7.getCounstructors（）；返回一个包含某些Constructor对象的数组
     * 8.getMothed：返回一个Method对象，此对象的形参类型为paramType
     * getDeclaredFields（）：返回Field对象的一个数组
     */
    public static void main(String[] args) throws ClassNotFoundException {
        Persion01 persion01 = new Persion01();
        System.out.println("这个人是：" + persion01.name);

        // 1
        Class c1 = persion01.getClass();
        // 2
        Class c2 = Class.forName("com.reflection.Student1");
        // 3
        Class<Student1> c3 = Student1.class;

        System.out.println(c1.hashCode());
        System.out.println(c2.hashCode());
        System.out.println(c3.hashCode());

        // 4
        Class c4 = Integer.TYPE;


        System.out.println(c1.getSuperclass());

    }
}

class Persion01 {
    String name;

    public Persion01() {
    }

    public Persion01(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Persion{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Student1 extends Persion01 {
    public Student1() {
        this.name = "学生";
    }
}

class Teacher extends Persion01 {
    public Teacher() {
        this.name = "老师";
    }
}