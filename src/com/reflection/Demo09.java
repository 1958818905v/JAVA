package com.reflection;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * @Description: TODO(反射操作泛型)
 * @Author lenovo
 * @Date 2021/8/11 17:03
 */
public class Demo09 {

    public void test01(Map<String, User> map, List<User> list) {
        System.out.println("test01");
    }

    public Map<String, User> test02() {
        System.out.println("test02");
        return null;
    }

    public static void main(String[] args) throws NoSuchMethodException {
        Method test01   = Demo09.class.getMethod("test01", Map.class, List.class);
        Type[] genericExceptionTypes = test01.getGenericExceptionTypes();
        for (Type type : genericExceptionTypes) {
            System.out.println(type);
        }
    }
}
