package com.oop.Demo06;
// 静态导入包

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/6/29 15:49
 */
public class Deemo01 {
    // 静态变量
    private static int age;

    static {
        // 静态代码块：只执行一次
        // 最先执行
    }

    // 非静态变量
    private double score;

    {
        // 匿名代码块：附初始值
        // 第二执行
    }

    public Deemo01() {
        // 构造方法
        // 最后执行
    }
}
