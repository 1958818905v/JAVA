package com.oop.Demo01;

/**
 * @Description: TODO(回顾方法)
 * @Author lenovo
 * @Date 2021/6/28 11:50
 */

public class Demo02 {
    public static void main(String[] args) {
        /*
         * 修饰符 返回值类型 方法名（）{
         *      方法体
         *      return；
         *    }
         * */
    }

    public String sayHello() {
        return "hello word!";
    }
}
