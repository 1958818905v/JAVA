package com.oop.Demo08;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/6/29 16:26
 */
public class Outer {

    private int id;

    public void out() {
        System.out.println("out");
    }


    class Inner {
        public void in() {
            System.out.println("in");
        }
    }

}
