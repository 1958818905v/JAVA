package com.oop.Demo07;

/**
 * @Description: TODO(接口)
 * @Author lenovo
 * @Date 2021/6/29 16:09
 */
public class Demo02 {
    /*
    分类：
        1.普通类：只有具体的实现
        2.抽象类：具体实现和规范都有（抽象方法）！
        3.接口：只有规范！自己无法写方法 约束和实现分离
     本质：
        1.就是规范
        2.是契约
        3.面对对象的精髓
      接口都需要实现类
      类可以实现接口 implements
      接口可以伪多继承
      接口里面定义属性都是常量
     */
}
