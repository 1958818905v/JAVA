package com.oop.Demo05;

/**
 * @Description: TODO(多态)
 * @Author lenovo
 * @Date 2021/6/29 14:43
 */
public class Demo01 {
    public static void main(String[] args) {
        /*
        动态编译
        注意事项：
            1.多态是方法的多态，属性没有多态
            2.父类和子类，有联系
            3.存在的条件：继承关系，方法重写，父类的引用指向子类对象
         */

        // 一个对象的实际类型是确定的，可以指向的引用类型就不一样了
//        父类的引用可以指向子类


    }

}
