//package com.oop.Demo04;
//
///**
// * @Description: TODO(继承)
// * @Author lenovo
// * @Date 2021/6/28 16:12
// */
//public class Demo02 {
//    public static void main(String[] args) {
//        /*
//        对一批类的抽象
//        关键字：extends
//        子类继承父类就有全部的方法
//        在java中所有的类都直接或间接继承extend Object
//        只有单继承，不能多继承
//        ---------------------------------------------
//        super
//         */
//        Student1 s1 = new Student1();
//        s1.say();
//        System.out.println(s1.getMoney());
//        Teacher t1 = new Teacher();
//        t1.test("GAo");
//    }
//}
