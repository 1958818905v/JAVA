package com.oop.Demo04;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/6/28 16:15
 */
public class Teacher extends Person {
    private String name = "123";

    public void test(String name) {
        System.out.println(name);
        System.out.println(this.name);
        System.out.println(super.name);
    }
}
