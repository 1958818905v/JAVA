package com.oop.Demo04;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/6/28 15:43
 */
public class Student {
    //    属性私有
    /*
    get set 操作属性
    规避不合法数据
    意义：
            1.提高程序安全性
            2.隐藏代码细节
            3.统一接口
            4.增加可维护性
     */

    private String name;
    private int id;
    private char sex;
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > 120 || age < 0) {
            this.age = 3;
        } else {
            this.age = age;

        }
    }
//set设置数据 get获取数据

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public void study() {

    }

    public void sleep() {

    }
}
