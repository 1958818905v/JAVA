package com.oop.Demo04;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/6/28 16:14
 */
public class Person {
    protected String name = "GAO";
    private int money = 1_0000_0000;

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void say() {
        System.out.println("123");
    }

}
