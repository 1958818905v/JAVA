package com.oop.Demo04;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/6/29 14:27
 */
public class A1 extends A2 {
    /*
    都是方法的重写和属性无关
    前提：
        继承关系，子类重写父类的方法
    条件：
        1.非静态
        2.public
     重写：
        1.方法名一样
        2.参数列表相同
        3.修饰符：范围可以扩大但不能缩小
        4.抛出的异常：范围可以缩小但是不可以扩大
        子类的方法和父类必须一致，方法体可以不同
      为什么要重写
        1.子类不需要或不满足

     */

    @Override
    public void test() {
        super.test();
    }
}
