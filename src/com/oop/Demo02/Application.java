package com.oop.Demo02;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/6/28 14:54
 */
public class Application {
    //    一个项目只要一个main方法
    public static void main(String[] args) {

        Person P1 = new Person("123");
        System.out.println(P1.name);

    }
}
/*
       类是抽象的 需要实例化
        类实例化会返回一个自己的对象
    student s1 = new student();
        s1.name = "123";
                s1.age = 18;
                student s2 = new student();
                System.out.println(s1.name);
                System.out.println(s1.age);*/
