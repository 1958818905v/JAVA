package com.oop.Demo02;

/**
 * @Description: TODO(构造函数)
 * @Author lenovo
 * @Date 2021/6/28 15:03
 */
public class Person {
// 一个类什么都不写 ，也会有一个方法
//    1.和类的名字相同

    String name;

    /*
     * 无参构造器
     *  1.使用new本质在调用构造器
     *  2.构造器用来初始化值
     * */

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }
}
/*
 * 构造器:
 *    1.和类名相同
 *    2.没返回值
 * 作用：
 *   1.new 本质在调用构造方法
 *   2.初始化对象的值
 * 注意点
 *   1.定义有参构造，一定要显示定义无参构造
 * this.***   :是代表当前类
 * */