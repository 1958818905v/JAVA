package com.CollectionFrame.set;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * @Description: TODO(Comparator接口)
 * @Author lenovo
 * @Date 2021/7/20 10:51
 */
public class Demo06 {
    /**
     * Comparator接口：实现定制比较（比较器）
     * Comparable接口：可比较的
     */
    public static void main(String[] args) {
        TreeSet<Persion> treeSet = new TreeSet<>(new Comparator<Persion>() {
            @Override
            public int compare(Persion o1, Persion o2) {
                int n1 = o1.getAge() - o2.getAge();
                int n2 = o1.getName().compareTo(o2.getName());
                return n1 == 0 ? n2 : n1;
            }
        });
        Persion p1 = new Persion("123", 20);
        Persion p2 = new Persion("123", 13);
        Persion p3 = new Persion("123", 14);
        treeSet.add(p1);
        treeSet.add(p2);
        treeSet.add(p3);
        System.out.println(treeSet.toString());
    }
}
