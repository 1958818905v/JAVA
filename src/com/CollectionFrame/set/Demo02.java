package com.CollectionFrame.set;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @Description: TODO(HashSet)
 * @Author lenovo
 * @Date 2021/7/20 9:10
 */
public class Demo02 {
    /**
     * 存储结构：哈希表(数组+链表+红黑树)
     * 存储过程：·基于HashCode计算元素存放的位置
     * ·当存入元素的哈希码相同时，会调用equals进行确认，如结果为true，则拒绝存入后者
     */
    public static void main(String[] args) {
        HashSet<Integer> hashSet = new HashSet();
        hashSet.add(1);
        hashSet.add(2);
        hashSet.add(3);
        System.out.println(hashSet.toString());
        hashSet.remove(1);
        System.out.println(hashSet.toString());

        for (Integer it : hashSet
        ) {
            System.out.println(it);

        }

        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

    }
}
