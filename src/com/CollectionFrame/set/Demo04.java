package com.CollectionFrame.set;

import java.util.Iterator;
import java.util.TreeSet;

/**
 * @Description: TODO(TreeSet)
 * @Author lenovo
 * @Date 2021/7/20 10:19
 */
public class Demo04 {
    /**
     * 存储结构：红黑树
     * 特点：
     * ·基于排序实现元素不重复
     * ·实现了SortedSet接口，对集合元素自动排序
     * ·元素对象的类型必须实现Comparable接口，指定排序规则
     * ·通过CompareTo方法确定是否为重复元素
     *
     * @param args
     */
    public static void main(String[] args) {
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(3);
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(4);
        System.out.println(treeSet.toString());
        treeSet.remove(4);
        System.out.println(treeSet.toString());

        Iterator it = treeSet.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
