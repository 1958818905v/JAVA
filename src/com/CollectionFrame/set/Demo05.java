package com.CollectionFrame.set;

import java.util.Iterator;
import java.util.TreeSet;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/20 10:37
 */
public class Demo05 {
    public static void main(String[] args) {
        /**
         * 要求：元素要实现Comparable
         */
        TreeSet<Persion> persion1 = new TreeSet<>();
        Persion p1 = new Persion("123", 12);
        Persion p2 = new Persion("123", 13);
        Persion p3 = new Persion("123", 14);
        persion1.add(p1);
        persion1.add(p2);
        persion1.add(p3);
        System.out.println(persion1.toString());
        persion1.remove(p1);
        System.out.println(persion1.toString());
        Iterator it = persion1.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
