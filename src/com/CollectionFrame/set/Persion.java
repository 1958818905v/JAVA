package com.CollectionFrame.set;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/20 9:30
 */
public class Persion implements Comparable<Persion> {
    private String name;
    private int age;

//    @Override
//    public int hashCode() {
//        int n1 = this.name.hashCode();
//        int n2 = this.age;
//
//        return n1 + n2;
//
//    }

    public Persion(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Persion persion = (Persion) o;

        if (age != persion.age) {
            return false;
        }
        return name != null ? name.equals(persion.name) : persion.name == null;
    }

    @Override
    public int hashCode() {
        /**
         * 31:  ·31是一个质数，减少散列冲突
         *      ·31可以提高执行效率 31*i=（i<<5）-i
         */
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (o == null) {
//            return false;
//        }
//        if (o instanceof Persion) {
//            Persion persion = (Persion) o;
//            if (this.name.equals(persion.getName()) && this.age == persion.getAge()) {
//                return true;
//            }
//        }
//        return super.equals(o);
//    }

    @Override
    public String toString() {
        return "Persion{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 比较规则：
     * 先按照姓名比，在按照年龄比
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Persion o) {
        int n1 = this.getName().compareTo(o.getName());
        int n2 = this.age - o.getAge();

        return n1 == 0 ? n2 : n1;
    }
}
