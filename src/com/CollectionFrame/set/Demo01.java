package com.CollectionFrame.set;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @Description: TODO(set)
 * @Author lenovo
 * @Date 2021/7/20 8:58
 */
public class Demo01 {
    /**
     * 特点：无序、无下标、元素不可重复
     * 方法：全部继承Collection方法
     * 实现类：
     * HashSet：
     * TreeSet：
     *
     * @param args
     */
    public static void main(String[] args) {

        Set<String> set = new HashSet();
        set.add("123");
        set.add("456");
        set.add("789");
        System.out.println(set.toString());

        set.remove("123");
        System.out.println(set.toString());

        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
        System.out.println(set.contains("123"));
        System.out.println(set.isEmpty());
    }
}
