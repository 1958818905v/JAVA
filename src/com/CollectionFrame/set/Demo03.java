package com.CollectionFrame.set;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/20 9:29
 */
public class Demo03 {
    public static void main(String[] args) {
        HashSet<Persion> persion = new HashSet<>();
        Persion p1 = new Persion("123", 12);
        Persion p2 = new Persion("123", 13);
        Persion p3 = new Persion("123", 14);
        persion.add(p1);
        persion.add(p2);
        persion.add(p3);
        persion.add(new Persion("123", 12));
        System.out.println(persion.toString());
        System.out.println(persion.size());
        persion.remove(p2);
        System.out.println(persion.toString());

        Iterator<Persion> iterator = persion.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }


    }
}
