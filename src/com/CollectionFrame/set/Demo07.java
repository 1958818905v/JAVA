package com.CollectionFrame.set;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/20 10:58
 */
public class Demo07 {
    /**
     * 使用TreeSet集合实现字符串按照长度进行排序
     */
    public static void main(String[] args) {
        TreeSet<String> ts = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                int n1 = o1.length() - o2.length();
                int n2 = o1.compareTo(o2);

                return n1 == 0 ? n2 : n1;
            }
        });
        ts.add("asfaaaa");
        ts.add("zxcf");
        ts.add("asdfgh");
        System.out.println(ts.toString());
    }
}
