package com.CollectionFrame.Map;

import java.util.TreeMap;

/**
 * @Description: TODO(TreeMap)
 * @Author lenovo
 * @Date 2021/7/21 15:04
 */
public class Demo04 {
    /**
     * 存储结构：红黑树
     * ·实现了SortedMap接口（是Map的子接口），可以key自动排序
     */
    public static void main(String[] args) {
        TreeMap<Student, String> treeMap = new TreeMap<Student, String>();
//        Student s1 = new Student(1,"a");
//        Student s2 = new Student(2,"b");
//        Student s3 = new Student(3,"c");
//        Student s4 = new Student(4,"d");
//        treeMap.put("电子",s1);
//        treeMap.put("商贸",s2);
//        treeMap.put("机电",s3);
//        treeMap.put("化工",s4);
//        System.out.println(treeMap.toString());
    }
}
