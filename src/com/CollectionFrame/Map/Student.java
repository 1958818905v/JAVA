package com.CollectionFrame.Map;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/21 10:51
 */
public class Student {
    // implements Comparable
    private String name;
    private int stuNo;

    public Student() {
    }

    public Student(int stuNo, String name) {
        this.name = name;
        this.stuNo = stuNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Student student = (Student) o;

        if (stuNo != student.stuNo) {
            return false;
        }
        return name != null ? name.equals(student.name) : student.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + stuNo;
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStuNo() {
        return stuNo;
    }

    public void setStuNo(int stuNo) {
        this.stuNo = stuNo;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", stuNo=" + stuNo +
                '}';
    }

//    @Override
//    public int compareTo(Object o) {
//
//        int n2 = this.stuNo-o.getStuNO();
//        return n2;
//    }
}
