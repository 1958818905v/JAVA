package com.CollectionFrame.Map;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @Description: TODO(Collections工具类)
 * @Author lenovo
 * @Date 2021/7/21 15:14
 */
public class Demo05 {
    /**
     * 集合工具类，定义了除了存取以外的集合常用方法
     * 方法：
     * ·reverse（）反转集合中的元素的顺序
     * ·shuffle（）随机重置集合元素的顺序
     * ·sort（）升序排序（元素类型必须实现Comparable接口）
     */
    public static void main(String[] args) {
        List<Integer> i = new ArrayList<>();
        i.add(1);
        i.add(2);
        i.add(3);
        i.add(4);
        Collections.reverse(i);
        System.out.println(i.toString());

        /**
         * binarySearch
         */
        int i1 = Collections.binarySearch(i, 1);
        System.out.println(i1);

        /**
         * copy
         * 复制需要俩个集合一样长
         */
        List<Integer> i3 = new ArrayList<>();
        for (int j = 0; j < i.size(); j++) {
            i3.add(0);
        }
        Collections.copy(i3, i);

        // List转为数组
        Integer[] arr = i.toArray(i.toArray(new Integer[0]));
        System.out.println(arr.getClass());

        // 数组转为集合(不能添加和删除元素)
        String[] names = {"a", "b", "c"};
        List<String> list = Arrays.asList(names);


    }
}
