package com.CollectionFrame.Map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @Description: TODO(map)
 * @Author lenovo
 * @Date 2021/7/20 11:02
 */
public class Demo01 {
    /**
     * Map实现类：
     * ·HashMap
     * ··
     * ·SortedMap
     * TreeMap
     * 特点：
     * ·存储任意键值对
     * ·键：无序、无下标、不允许重复
     * ·值：无序、无下标、允许重复
     * 方法：
     * ·put（）添加数据
     * ·get（）根据键获取相应的值
     * ·Set（）返回所有的Key
     * ·values（）返回所有值的Collection集合
     * ·Set<Map.Entry<K,V>> 键值匹配的Set集合
     * 添加重复元素，后面的覆盖掉前面的
     */
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("1", "1");
        map.put("2", "1");
        map.put("3", "1");
        System.out.println(map.toString());

        map.remove("1");
        System.out.println(map.toString());

        // 遍历
        Set<String> keyset = map.keySet();
        for (String key : keyset) {
            System.out.println(key + map.get(key));
        }
        // 效率高与上面
        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        for (Map.Entry<String, String> entry : map.entrySet()
        ) {
            System.out.println(entry.getKey() + "......" + entry.getValue());
        }
        Iterator it = entrySet.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
