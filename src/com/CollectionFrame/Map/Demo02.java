package com.CollectionFrame.Map;

import java.util.HashMap;

/**
 * @Description: TODO(hashMap)
 * @Author lenovo
 * @Date 2021/7/21 9:20
 */
public class Demo02 {
    /**
     * 线程不安全，运行效率快，允许用null作为Key或Value的值
     * 初始容量：16
     * 默认加载因子：0.75
     * 存储结构：哈希表
     * 使用key可hashcode和equals作为重复值
     * <p>
     * 总结：
     * ·hashmap刚创建是，table是null，为了节省空间，table添加第一个元素时容量调整为16
     * ·当元素达到容量的0.75是会进行扩容，扩容后调整为原来的俩倍，目的是减少调整元素个数
     * ·1.8之后：
     * ··每个链表长度大于8，并且数组元素个数大于等于64时，会调整为红黑树
     * ··当链表长度小于6时，调整成链表
     * ·1.8之前：
     * ··链表是头插入，1.8之后链表尾插入
     */
    public static void main(String[] args) {
        HashMap students = new HashMap<Student, String>();
        Student s1 = new Student(1, "a");
        Student s2 = new Student(2, "b");
        Student s3 = new Student(3, "c");
        Student s4 = new Student(4, "d");
        students.put("电子", s1);
        students.put("商贸", s2);
        students.put("机电", s3);
        students.put("化工", s4);
        System.out.println(students.toString());
        students.remove(s1);
        System.out.println(students.toString());


    }
}
