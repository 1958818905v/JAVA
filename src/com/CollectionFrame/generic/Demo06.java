package com.CollectionFrame.generic;

import java.util.ArrayList;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/19 17:46
 */
public class Demo06 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
//        list.add(12);
        list.add("12");
//        list.add(false);
        list.add("a");
        System.out.println(list.toString());
        for (String obj : list
        ) {
            System.out.println(obj);

        }
    }
}
