package com.CollectionFrame.generic;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/19 15:58
 */
public class Demo01 {
    /**
     * 泛型：本质是参数化类型，把类型作为参数传递
     * 常见形式：泛型类、泛型接口、泛型方法
     * 语法：<T,....>T(表示占位符，表示一种引用类型)
     * 好处：1、提高代码的重要性
     *      2、防止类型异常转换，提高代码的安全性
     * @param args
     */

}
