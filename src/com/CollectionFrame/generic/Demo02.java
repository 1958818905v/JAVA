package com.CollectionFrame.generic;

import java.util.LinkedList;

/**
 * @Description: TODO(泛型类)
 * @Author lenovo
 * @Date 2021/7/19 17:11
 */
public class Demo02<T> {
    /**
     * 语法：类名<T>
     * T表示占位符，表示引用类型
     * 不能直接实例化，创建对象
     */
    // 创建变量
    T t;
    LinkedList<String> ls = new LinkedList();

    // 创建方法(作为参数)
    public void show(T t) {
        System.out.println(t);
    }

    // 使用泛型作为方法返回值
    public T getT() {
        return t;
    }


}
