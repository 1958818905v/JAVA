package com.CollectionFrame.generic;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/19 17:15
 */
public class Test {
    public static void main(String[] args) {
        Demo02<String> demo02 = new Demo02<>();
        demo02.t = "12212";
        demo02.show("123");
        String s = demo02.getT();

        Demo02<Integer> demo03 = new Demo02<>();
        demo03.t = 666;
        demo03.show(3);
        int i = demo03.getT();


        Demo04 demo04 = new Demo04();
        demo04.server("s");

        Demo05 demo05 = new Demo05();
        demo05.show(1);
    }
}
