package com.gao.base;

/**
 * @Description: TODO(逻辑运算符)
 * @Author lenovo
 * @Date 2021/6/22 8:54
 */
public class Demo4 {
    public static void main(String[] args) {
//        与（and）、或（or）、非(取反)
        boolean a = true;
        boolean b = false;

        System.out.println("a && b : " + (a && b));
//        逻辑与运算：俩个变量都为真，结果为true 否则为false
        System.out.println("a || b:" + (a || b));
//        逻辑或运算：俩个变量有一个为真，结果为true 否则为false
        System.out.println("!(a &&b):" + !(a && b));
//        去反数
//        短路运算
//        前面为错不执行后面的（直接短路），只有第一个为真才执行后面
        int c = 5;
        boolean d = (c < 4) && (c++ < 4);
        System.out.println(d);
        System.out.println(c);
    }
}
