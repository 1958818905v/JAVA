package com.gao.base;

public class Demo3 {
    public static void main(String[] args) {
//        整数拓展

        int i = 10;
        int i2 = 010;
        int i3 = 0x10;
        System.out.println(i);
        System.out.println(i2);
        System.out.println(i3);
//        浮点数拓展
        float f = 0.1f;
        double d = 1.0 / 10;
        System.out.println(d == f);
        System.out.println(f);
        System.out.println(d);
//        字符拓展

    }
}
