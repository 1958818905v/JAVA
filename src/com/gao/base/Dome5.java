package com.gao.base;

/**
 * @Description: TODO(位运算)
 * @Author lenovo
 * @Date 2021/6/22 9:24
 */
public class Dome5 {
    public static void main(String[] args) {
        /*
         * A = 0011 1100
         * B = 0000 1101
         *----------------------------------
         * A&B = 0000 1100 都为1取1 否则为0
         * A||B = 0011 1101 都为0取0 否则为1
         * A^B = 0011 0001 相同为0 不同为1
         * ~B = 1111 0010 取反
         *
         * 2*8 = 16 怎么运算最快？
         * << 数字乘二
         * >> 数字除二
         *  */

        System.out.println(2 << 3);
    }
}
