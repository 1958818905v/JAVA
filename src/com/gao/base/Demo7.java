package com.gao.base;

/**
 * @Description: TODO(包机制)
 * @Author lenovo
 * @Date 2021/6/22 9:41
 */
public class Demo7 {
    public static void main(String[] args) {
//        包的本质就是文件夹
        /*
         * package 开头 必须放在最上面
         * 一般用公司域名倒置作为包名
         * 使用import导入
         * */
    }
}
