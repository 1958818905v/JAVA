package com.gao.method;

/**
 * @Description: TODO(方法详述)
 * @Author lenovo
 * @Date 2021/6/22 17:34
 */
public class Demo01 {
    //    main方法
    public static void main(String[] args) {
        /*
         * 方法是语句的集合，他们在一起执行一个方法
         *
         * 解决一类问题的有序组合
         * 方法包含在类或对象中
         * 方法在程序中创建，在其他地方引用
         *
         * -----------------------------
         * 设计方法原则
         * 方法的本意是功能块，实现某个功能的语句块的集合
         * 设计时保持方法的原子性，一个方法只完成一个功能，有利于后期扩展
         *
         * -------------------------------------------------------
         * 定义
         *
         * 类似于其他语言的函数
         * 修饰符 返回值类型 方法名（参数类型 参数名）{
         *      方法体
         *      return 返回值;
         *    }
         * */
//        实参
        int sum = add(1, 2);
        System.out.println(sum);


    }

    /**
     * @param a
     * @param b
     * @return 形参
     */
    public static int add(int a, int b) {
        return a + b;
    }
}
