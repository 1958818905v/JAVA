package com.gao.method;

/**
 * @Description: TODO(比大小)
 * @Author lenovo
 * @Date 2021/6/22 17:54
 */
public class Demo02 {
    public static void main(String[] args) {
        /*
         * 方法调用
         * */
        int max = Max(10, 10);
        System.out.println(max);

    }

    /**
     * @param num1
     * @param num2
     * @return return 可以终止方法
     */

    public static int Max(int num1, int num2) {
        int result = 0;
        if (num1 == num2) {
            System.out.println("相等");
            return 0;
        }
        if (num1 > num2) {
            result = num1;
        } else {
            result = num2;
        }
        return result;
    }
}
