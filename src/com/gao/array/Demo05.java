package com.gao.array;

import java.util.Arrays;

/**
 * @Description: TODO(Arrays类)
 * @Author lenovo
 * @Date 2021/6/28 10:34
 */
public class Demo05 {
    public static void main(String[] args) {
        int[] a = {1, 2, 9563, 752896, 95863, 8547};

//        排序
        Arrays.sort(a);
        //打印数组元素
        System.out.println(Arrays.toString(a));
        Arrays.fill(a, 2, 3, 0);
    }

}
