package com.gao.array;

import java.util.Arrays;

/**
 * @Description: TODO(冒泡排序)
 * @Author lenovo
 * @Date 2021/6/28 10:47
 */
public class Demo06 {
    public static void main(String[] args) {
        int[] a = {9, 5, 8, 1, 3, 6, 8};
        int[] sort = sort(a);
        System.out.println(Arrays.toString(sort));
    }

    //    冒泡排序
//    1.比较数组中，俩个相邻的元素，如果第一个比第二大，就交换位置
//    2.每次一都会产生一个最大或最小的数字
//    3.下一轮减少一次
    public static int[] sort(int[] array) {
        int temp = 0;
//        优化 减少没有意义的比较
        Boolean flag = false;
//        判断走多少次
        for (int i = 0; i < array.length - 1; i++) {
//            内层循环，判断俩数，第一个数比第二个大就交换位置
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j + 1] > array[j]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    flag = true;
                }
//                优化
                if (flag == false) {
                    break;
                }
            }
        }
        return array;
    }
}
