package com.gao.array;

/**
 * @Description: TODO(数组的使用)
 * @Author lenovo
 * @Date 2021/6/28 9:47
 */
public class Demo03 {
    public static void main(String[] args) {
        int[] arrays = {1, 2, 3, 4, 5};
//        遍历
        for (int i = 0; i <= arrays.length - 1; i++) {
            System.out.println(arrays[i]);
        }
//        计算和
        int sum = 0;
        for (int i = 0; i <= arrays.length - 1; i++) {
            sum = sum + arrays[i];
            System.out.println(sum);
        }
//        查找最大元素
        int max = arrays[0];
        for (int i = 1; i < arrays.length; i++) {
            if (arrays[i] > max) {
                max = arrays[i];
            }
            System.out.println(max);
        }
//for-each
//        适合打印输出(取不到下标)
        for (int array : arrays) {
            System.out.println(array);
        }

        printArray(arrays);
        printArray(reverse(arrays));
    }

    //打印数组元素
    public static void printArray(int[] arrays) {
        for (int i = 0; i < arrays.length; i++) {
            System.out.print(arrays[i] + " ");

        }
    }

    //    翻转数组
    public static int[] reverse(int[] arrays) {
        int[] result = new int[arrays.length];
        for (int i = 0, j = result.length - 1; i < arrays.length; i++, j--) {
            result[j] = arrays[i];
        }


        return result;
    }

}
