package com.gao.array;

/**
 * @Description: TODO(数组概述)
 * @Author lenovo
 * @Date 2021/6/28 9:08
 */
public class Demo01 {
    public static void main(String[] args) {
// 变量的类型 变量的名字 = 值；
//        数组类型[] 数组名；定义
        int[] nums1;
//        创建数组
        nums1 = new int[10];
//        数组赋值 默认值0
        nums1[0] = 1;
        nums1[1] = 2;
        nums1[2] = 3;
        nums1[3] = 4;
        nums1[4] = 5;
        nums1[5] = 6;
        nums1[6] = 7;
        nums1[7] = 8;
        nums1[8] = 9;
        nums1[9] = 10;
//        下表从0开始
//        数组名.length 长度

        int sum = 0;
        for (int i = 0; i < nums1.length; i++) {
            sum = sum + nums1[i];
        }

        System.out.println(sum);


//        静态初始化（创建+赋值）
//        固定不可改变
        int[] a = {1, 2, 3, 4, 5};

//        动态初始化(包含默认初始化)
        int[] b = new int[20];
        b[1] = 12;

    }
}
