package com.gao.array;

/**
 * @Description: TODO(稀疏数组)
 * @Author lenovo
 * @Date 2021/6/28 11:12
 */
public class Demo07 {
    public static void main(String[] args) {
        /*
         * 处理方式
         * 1.记录几行几列
         * 2.不同的行列及值记在一个小规模的数组中
         *
         * */
        int[][] checkerboard = new int[11][11];
        checkerboard[1][2] = 1;
        checkerboard[2][3] = 2;
        for (int[] ints : checkerboard) {
            for (int anInt : ints) {
                System.out.print(anInt + "\t");

            }
            System.out.println();
        }

//        转换稀疏数组
//        获取有效值的个数
        int sum = 0;
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
                if (checkerboard[i][j] != 0) {
                    sum++;
                }
            }
        }
        System.out.println("获取有效个数：" + sum);
        int[][] checkerboard1 = new int[sum + 1][3];
        checkerboard1[0][0] = 11;
        checkerboard1[0][1] = 11;
        checkerboard1[0][2] = sum;
//        遍历二维数组，将非零的值，存放稀疏数组中
        int count = 0;
        for (int i = 0; i < checkerboard.length; i++) {
            for (int j = 0; j < checkerboard[i].length; j++) {
                if (checkerboard[i][j] != 0) {
                    count++;
                    checkerboard1[count][0] = i;
                    checkerboard1[count][1] = j;
                    checkerboard1[count][2] = checkerboard[i][j];
                }
            }
        }
//        输出
        for (int i = 0; i < checkerboard1.length; i++) {
            System.out.println(checkerboard1[i][0] + "\t" +
                    checkerboard1[i][1] + "\t"
                    + checkerboard1[i][2]);
        }
    }
}
