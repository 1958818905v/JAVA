package com.gao.control;

/**
 * @Description: TODO(循环结构)
 * @Author lenovo
 * @Date 2021/6/22 15:54
 */
public class Demo10 {
    public static void main(String[] args) {
        /*
         * while循环
         * 最基本的循环
         * while（布尔表达式）{
         *   循环内容
         *  }
         * */

        int num = 0;
        int sum = 0;
        while (num <= 100) {
            System.out.println(num);
            sum = sum + num;
            num++;
        }
        System.out.println("和：" + sum);

    }
}
