package com.gao.control;

import java.util.Scanner;

/**
 * @Description: TODO(Scanner进阶使用)
 * @Author lenovo
 * @Date 2021/6/22 10:23
 */
public class Demo02 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);


//        键盘接收数据
        int i = 0;
        float f = 0f;

        System.out.println("请输入整数：");
        if (s.hasNextInt()) {
            i = s.nextInt();
            System.out.println(i);
        } else {
            System.out.println("不是整数！");
        }

        System.out.println("请输入小数：");
        if (s.hasNextFloat()) {
            f = s.nextFloat();
            System.out.println(f);
        } else {
            System.out.println("不是小数！");
        }

        s.close();
    }
}
