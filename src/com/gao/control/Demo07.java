package com.gao.control;

import java.util.Scanner;

/**
 * @Description: TODO(if多选择结构)
 * @Author lenovo
 * @Date 2021/6/22 15:24
 */
public class Demo07 {
    public static void main(String[] args) {
        /*
         *   if （布尔表达式1）{
         *       表达式1为true执行
         *       }else if（布尔表达式2）{
         *       表达式1为true执行
         *       }else{
         *       以上代码都不为true执行
         *   }
         *
         * if 语句至多有一个else语句。else语句在所有else if语句之后
         * if 语句可以有多个else if语句，但else必须在所有else if之后
         * 一旦有一个else if 语句检测为true ，其他的else if 以及else语句都跳过执行
         *
         * */
        int score = 0;

        Scanner s = new Scanner(System.in);
        System.out.println("请输入成绩：");
        score = s.nextInt();

        if (score == 100) {
            System.out.println("满分");
        } else if (score >= 80 && score < 100) {
            System.out.println("A");
        } else if (score >= 60 && score < 80) {
            System.out.println("B");
        } else {
            System.out.println("不合格");
        }
    }
}
