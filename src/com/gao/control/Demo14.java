package com.gao.control;

/**
 * @Description: TODO(九九乘法表)
 * @Author lenovo
 * @Date 2021/6/22 16:33
 */
public class Demo14 {
    public static void main(String[] args) {
/*        for (int j = 0; j <= 9; j++) {
            for (int i = 1; i <= j; i++) {
                System.out.print(j + "*" + i + "=" + (j * i) +"\t");
            }
            System.out.println();
        }*/

        for (int j = 1; j <= 9; j++) {
            for (int i = 1; i <= j; i++) {
                System.out.print(j + "*" + i + "=" + (j * i) + "\t");
            }
            System.out.println();
        }

        /*
         * 里面的for循环是输出1*1=1
         * 外面的for循环控制一行有几个
         * i<=9是为了控制一行有几个
         * */
    }
}
