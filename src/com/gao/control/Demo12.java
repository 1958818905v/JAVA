package com.gao.control;

/**
 * @Description: TODO(For 循环)
 * @Author lenovo
 * @Date 2021/6/22 16:08
 */
public class Demo12 {
    public static void main(String[] args) {
        /*
         *
         * for(初始化;布尔表达值;更新){
         *   代码
         *  }
         *
         * */

        int oddSum = 0;
        int evenSum = 0;
//        1-100奇数和偶数的和
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0) {
                oddSum += i;
            } else {
                evenSum += i;
            }
        }
        System.out.println(oddSum);
        System.out.println(evenSum);

//1-1000能被5整除
        for (int i = 0; i < 1000; i++) {
            if (i % 5 == 0) {
                System.out.print(i + "\t");
            }
            if (i % (5 * 3) == 0) {
                System.out.println();
            }
        }

    }

}
