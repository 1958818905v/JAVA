package com.gao.control;

/**
 * @Description: TODO(Do while)
 * @Author lenovo
 * @Date 2021/6/22 16:02
 */
public class Demo11 {
    public static void main(String[] args) {
        /*
         * 如果条件不满足不能进入循环
         * do....while 至少会执行一次循环
         *-----------------------------
         * 不同点
         * while先判断后执行 do...while相反
         * Do....while 至少会执行一次
         *---------------------------------
         * do{
         *      代码语句；
         *  }while（布尔表达式）；
         * */


    }
}
