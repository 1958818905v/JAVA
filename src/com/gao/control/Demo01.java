package com.gao.control;

import java.util.Scanner;

/**
 * @Description: TODO(交互Scanner)
 * @Author lenovo
 * @Date 2021/6/22 10:05
 */
public class Demo01 {

    public static void main(String[] args) {
//        实例化对象
        Scanner s = new Scanner(System.in);
        System.out.println("使用nextLine方法接收：");
//        判断有没有输入字符串
        if (s.hasNextLine()) {
//            使用next方法接收
            String str = s.nextLine();
//            程序会等待用户输入完毕
            System.out.println(str);
        }
//属于IO流的类如果不关闭会一直占用资源，养成用完就关
        s.close();
    }
    /*
     * next():
     * 1.一定要读取到有效字符串后才可以结束输入；
     * 2.对输入有效字符之前遇到的空白，next（）方法会自动将其去掉
     * 3.只有输入有效字符串后才能将其后面的空白作为分隔符或结束符
     * 4.next（）不能得到带有空格的字符串
     *
     * ------------------------------------------------------------
     *
     * nextLine():
     * 1.以Enter为结束符，也就是说nextLine（）方法返回的是输入回车之前的所有字符
     * 2.可以获取空白
     * */
}
