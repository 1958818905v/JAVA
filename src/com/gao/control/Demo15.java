package com.gao.control;

/**
 * @Description: TODO(增强for循环)
 * @Author lenovo
 * @Date 2021/6/22 16:48
 */
public class Demo15 {
    public static void main(String[] args) {
        /*
         * for(声明语句：表达式){
         *   语句
         *  }
         * */
//遍历数组的元素
        int[] numbers = {10, 20, 30};
        for (int x : numbers) {
            System.out.println(x);
        }
    }
}
