package com.gao.control;

import java.util.Scanner;

/**
 * @Description: TODO(Scanner练习)
 * @Author lenovo
 * @Date 2021/6/22 10:33
 */
public class Demo03 {
    public static void main(String[] args) {
//        输入多个数字，并求其总和与平均数，每输一个数字用回车确认，通过输入非数字来结束输入并输出执行结果
        Scanner s = new Scanner(System.in);

        double sum = 0;
        double age = 0;
        int m = 0;
        System.out.println("请输入整数：");
        while (s.hasNextDouble()) {
            double x = s.nextDouble();
            sum = sum + x;
            age = sum / m;
            m++;

        }
        System.out.println("平均数为：" + age);
        System.out.println("和为：" + sum);
        s.close();

    }
}
