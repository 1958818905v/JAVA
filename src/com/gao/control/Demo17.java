package com.gao.control;

/**
 * @Description: TODO(练习 ( 三角形) debug)
 * @Author lenovo
 * @Date 2021/6/22 17:17
 */
public class Demo17 {
    public static void main(String[] args) {
        for (int i = 1; i <= 5; i++) {
            for (int j = 5; j >= i; j--) {
                System.out.print(" ");
            }
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            for (int j = 1; j < i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
