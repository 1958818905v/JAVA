package com.gao.control;

import java.util.Scanner;

/**
 * @Description: TODO(选择结构)
 * @Author lenovo
 * @Date 2021/6/22 10:49
 */
public class Demo05 {
    public static void main(String[] args) {
        /*
         * if单选择
         * if（布尔表达式）{
         * 为true执行
         * }
         * */
        Scanner s = new Scanner(System.in);
        System.out.println("请输入内容：");
        String str = s.nextLine();
//       equals() 判断字符串是否相等
        if (str.equals("Hello")) {
            System.out.println(str);

        }
        System.out.println("End");

        s.close();

    }
}
