package com.gao.control;

import java.util.Scanner;

/**
 * @Description: TODO(if双选择结构)
 * @Author lenovo
 * @Date 2021/6/22 10:55
 */
public class Demo06 {
    public static void main(String[] args) {
        /*
         * if双选择
         * if（布尔表达式）{
         *      为true执行
         * } else {
         *      为false执行
         * }
         * */
        double score = 0;
        int pass = 60;
        Scanner s = new Scanner(System.in);
        System.out.println("请输入你的成绩：");
        score = s.nextDouble();
        if (score >= pass) {
            System.out.println("及格");
        } else {
            System.out.println("不合格");
        }
        s.close();
    }
}
