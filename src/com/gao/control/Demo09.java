package com.gao.control;

/**
 * @Description: TODO(switch 匹配字符串)
 * @Author lenovo
 * @Date 2021/6/22 15:47
 */
public class Demo09 {
    public static void main(String[] args) {
        /*
         * 字符的本质还是数字
         * 反编译  java-->class(字节码源文件)-->反编译（IDEA）
         * */
        String name = "gao";
        switch (name) {
            case "shi":
                System.out.println("猜错了！");
                break;
            case "gao":
                System.out.println("猜对了！");
                break;
            default:
                System.out.println("不对！");
                break;
        }
    }
}
