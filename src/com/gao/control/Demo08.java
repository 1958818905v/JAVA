package com.gao.control;

/**
 * @Description: TODO(Switch 多选择结构)
 * @Author lenovo
 * @Date 2021/6/22 15:36
 */
public class Demo08 {
    public static void main(String[] args) {
        /*
         * switch（expression）{
         *   case value：
         *       语句
         *     break;      （可选）
         *   case value：
         *       语句
         *       break;     （可选）
         *   default:      （可选）
         *     语句
         *   }
         *
         *
         * switch 匹配一个具体的值
         *  break 匹配上有break直接跳出
         *        如果不加会把下面所有的都穿透
         *
         *
         *
         * JDK7开始可以比较字符串
         * */

        char grade = 'C';
        switch (grade) {
            case 'A':
                System.out.println("优秀");
                break;
            case 'B':
                System.out.println("良好");
                break;
            case 'C':
                System.out.println("合格");
                break;
            default:
                System.out.println("不合格");
                break;
        }


    }
}
