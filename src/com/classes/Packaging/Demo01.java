package com.classes.Packaging;

/**
 * @Description: TODO(包装类)
 * @Author lenovo
 * @Date 2021/7/15 9:14
 */
public class Demo01 {
    /***
     * 包装类
     * 基本类型所对于的引用数据类型
     * 基本数据类型      引用数据类型           继承
     *     byte             Byte        java.lang.number
     *     int              Integer     java.lang.number
     *     short            Short       java.lang.number
     *     long             Long        java.lang.number
     *     double           Double      java.lang.number
     *     float            Float       java.lang.number
     *     char             Character       object
     *     boolean          Boolean         object
     *
     *     将基本类型（栈里）包装在对象中（堆里）
     *     类型转换装箱和拆箱
     *     类型转换：
     *      1.number类6个（拆箱）
     *          ·intValue（）  以int形式返回指定的数值
     *          ·其他六个同理（byte、double、float、int、long、short）
     *          ·valueOf（） （装箱）
     *      2.parseXXX()静态方法
     *          ·字符串和基本类型的转换
     *     装箱：栈里的数据放到堆里，基本类型转换成引用类型
     *     拆箱：堆里的数据放到栈里，引用类型转换为基本类型
     *
     *
     *
     *
     */
    public static void main(String[] args) {
        int num = 10;
        // 装箱(JDK1.5之前)
        Integer integer = new Integer(num);
        Integer integer1 = Integer.valueOf(num);
        // 拆箱(JDK1.5之前)
        Integer integer2 = Integer.valueOf(100);
        int num2 = integer2.intValue();

        // JDK1.5之后提供自动装箱和拆箱
        int age = 30;
        Integer integer3 = age;
        int age2 = integer1;


        // 基本数据类型转换字符串
        int n1 = 100;
        // 1.使用+号实现
        String s1 = n1 + "";
        System.out.println(s1.getClass());
        // 2.使用Integer中的toString（）方法
        String s2 = Integer.toString(n1);
        System.out.println(s2.getClass());
        // 3.重载方法(得到进制)
        String s3 = Integer.toString(n1, 8);
        System.out.println(s3);
        // 4.toBinary（）得到二进制

        //字符串转基本类型
        String str1 = "100";
        //1.使用Integer.parseXXX（）
        int n2 = Integer.parseInt(str1);
        System.out.println(n2);
        // boolean类字符串转换为boolean类型
        String str2 = "true";
        boolean flag = Boolean.parseBoolean(str2);
        System.out.println(flag);
    }


}
