package com.classes.Packaging;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/15 9:59
 */
public class Demo02 {
    /**
     * 整数缓存区：
     * java预先创建了256个常用的整数包装类型对象(-128~127)
     * 对已创建的对象进行复用
     */
    public static void main(String[] args) {
        // 面试题
        Integer integer = Integer.valueOf(100);
        Integer integer1 = Integer.valueOf(100);
        System.out.println(integer == integer1);


    }
}
