package com.classes.InnerClass;

/**
 * @Description: TODO(内部类)
 * @Author lenovo
 * @Date 2021/7/14 16:25
 */
public class Demo01 {
    /**
     * 内部类：一个类的内部在定义一个类
     * 特点：1.可以生产独立的字节码文件
     * 2.可以直接访问外部类的私有成员，不破坏封装性
     * 3.可以为外部类提供必要的内部组件
     * <p>
     * 内部类分类：
     * 1. 成员内部类
     * (1) 与实例变量和方法级别相同
     * （2） 必须依赖外部类
     * （3） 属性相同，调用外部类的必须（外部类.this.属性）
     * （4） 成员内部类不能定义静态属性
     * 2. 静态内部类
     * （1）不依赖外部类，直接创建，可以声明静态成员
     * （2）级别和外部类相同
     * 3. 局部内部类
     * （1）定义在外部类方法中，仅限于当前的方法
     * （2）不能包含静态变量
     * 4. 匿名内部类
     * （1）没有类名的局部内部类
     * （2）必须继承一个父类或者实现一个接口
     * （3）定义类实现类创建对象的合并，只能创建一个该类
     */
    private String name = "张三";
    private int age = 20;

    public void show() {
        // 局部内部类
        String name = "张三";
        int age = 20;
        class A1 {
            private String address = "内蒙古";
            private String iphone = "123456789";

            public void show1() {
                System.out.println(name);
                System.out.println(age);

                System.out.println(address);
                System.out.println(iphone);
            }
//            A1 a1 = new A1();
//            a1.show1();
        }
    }

    static class Inner {
        // 静态内部类
        private String address = "内蒙古";
        private String iphone = "123456789";

        public void show() {
            Demo01 demo01 = new Demo01();
            System.out.println(demo01.name);
            System.out.println(demo01.age);

            System.out.println(address);
            System.out.println(iphone);
        }
    }

    class Header {
        //成员内部类
        private String address = "内蒙古";
        private String iphone = "123456789";

        public void show() {
            System.out.println(name);
            System.out.println(age);

            System.out.println(address);
            System.out.println(iphone);
        }
    }

}
