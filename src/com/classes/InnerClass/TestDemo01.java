package com.classes.InnerClass;

/**
 * @Description: TODO(测试成员内部类)
 * @Author lenovo
 * @Date 2021/7/14 16:54
 */
public class TestDemo01 {
    public static void main(String[] args) {
        // 繁琐
        Demo01 d = new Demo01();
        Demo01.Header h = d.new Header();
        h.show();
        // 简单
        Demo01.Header h1 = new Demo01().new Header();
        h1.show();
    }
}
