package com.classes.InnerClass;

/**
 * @Description: TODO(匿名内部类测试)
 * @Author lenovo
 * @Date 2021/7/14 17:39
 */
public class TestDemo04 {
    public static void main(String[] args) {
        // 方法一
        //        USB usb = new Mouse();
        //        usb.service();

        class show implements USB {

            @Override
            public void service() {
                System.out.println("点击12");
            }
        }
        USB usb = new show();
        usb.service();


    }
}
