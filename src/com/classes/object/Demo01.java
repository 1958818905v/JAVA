package com.classes.object;

/**
 * @Description: TODO(OBJECT类)
 * @Author lenovo
 * @Date 2021/7/14 17:47
 */
public class Demo01 {
    /***
     * 超类、基类 位于继承树的最顶层
     * 是所有对象都具备的方法
     * Object类型可以存储任何对象
     *  ·作为参数，可接受任何对象
     *  ·作为返回值，可返回任何对象
     */
    public static void main(String[] args) {

        /**
         * getClass（）方法
         * 返回存储的实际对象类型
         * 判断俩个对象的存储对象是否一致
         */

        /**
         * hashCode（）方法
         * 返回该对象的哈希码值
         * 根据对象的地址或字符串或数字使用hash算法计算出来的int类型的数字
         * 一般相同对象返回相同的哈希码
         *
         * 判断是不是同一个对象
         */
        /**
         * toString()方法
         * 返回对象的字符串表示
         * 可以覆盖该方法
         */
        /**
         * equals()方法
         * 默认实现为（this == obj），比较俩个地址是否相同
         * 可覆盖比较俩个对象是否相同
         */
        /**
         * finalize（）方法
         * 垃圾对象：没有有效引用指向此对象，为垃圾对象
         * 垃圾回收：由GC销毁垃圾，释放数据存储空间
         * 自动回收机制：JVM的内存耗尽，一次性回收所有垃圾对象
         * 手动回收机制：使用System.gc（）；通知JVM执行垃圾回收
         */

    }
}
