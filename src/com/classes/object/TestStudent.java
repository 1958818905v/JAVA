package com.classes.object;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/14 17:53
 */
public class TestStudent {
    public static void main(String[] args) {
        // getclass()
        Student s1 = new Student("aaa", 12);
        Student s2 = new Student("bbb", 18);
        // 判断是不是同一个类型
        Class class1 = s1.getClass();
        Class class2 = s2.getClass();
        if (class1 == class2) {
            System.out.println("属于同一个类型");
        } else {
            System.out.println("不是同一个类型");
        }

        // hashCode()
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());

        // toString（）
        System.out.println(s1.toString());
        System.out.println(s2.toString());

        // 4.equals()
        System.out.println(s1.equals(s2));


    }
}
