package com.classes.object;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/14 17:52
 */
public class Student {
    private String name;
    private int age;


    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return name + age;
    }


    // equals 重写
    @Override
    public boolean equals(Object obj) {
        // 1.判断是否同一个引用
        if (this == obj) {
            return true;
        }
        // 2.判断obj是否等于null
        if (obj == null) {
            return false;
        }
        // 3. 判断是不是同一个类型
        //        if (this.getClass() == obj.getClass()){
        //
        //        }
        // instanceof判断对象是否是某种类型
        if (obj instanceof Student) {
            // 4.强制类型转换
            Student s = (Student) obj;
            // 5.比较
            if (this.name.equals(s.getName()) && this.age == s.getAge()) {
                return true;
            }
        }


        return false;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.name + "对象被回收");
    }
}
