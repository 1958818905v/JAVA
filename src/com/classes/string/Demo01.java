package com.classes.string;

import java.util.Arrays;
import java.util.Locale;

/**
 * @Description: TODO(字符串)
 * @Author lenovo
 * @Date 2021/7/15 10:14
 */
public class Demo01 {
    /**
     * 字符串是常量，创建之后不可改变
     * ·没有修改赋值，而是重新开辟了一块空间，如原来的只没有再用，就变为垃圾回收了
     * 字符串字面值存储在字符串池中，可以共享
     * 使用new会产生俩个对象，堆栈各有一个，栈里存的是堆里的地址
     */

    public static void main(String[] args) {
        String name = "hello";
        name = "helloword";
        String name2 = "helloword";
        // 方法使用
        /**
         *常用的方法：
         * ·length（）长度
         * ·charAt（）返回某个位置的字符
         * ·contains（）判断是否包含某个字符串
         * ·toCharArray（）字符串转换为数组
         * ·indexOf（）查找str首次出现的下标，存在返回下标，不存在返回-1
         * ·lastIndexOf（）查找字符串在当前字符串中最后一次出现的下标索引
         * ·trim（）去掉字符串前后得字符
         * ·toUpperCase（）将小写转换为大写
         * ·endsWith（String str）判断字符串是否以str结尾
         * ·startsWith（String str）判断字符串是否以str开头
         * ·replace（char oldChar，char newChar）将旧字符替换为新字符
         * split(String str) 根据str做拆分
         * 补充：
         *  ·equals（）比较相等
         *  ·compareTo（）比较大小（字符表里的顺序）
         *
         */
        String content = " java是世界上 最好的语言！ ";
        System.out.println(content.length());
        System.out.println(content.charAt(content.length() - 1));
        System.out.println(content.contains("java"));
        System.out.println(Arrays.toString(content.toCharArray()));
        System.out.println(content.indexOf("j"));
        System.out.println(content.lastIndexOf("a", 2));
        System.out.println(content.trim());
        System.out.println(content.toUpperCase(Locale.ROOT));
        System.out.println(content.startsWith(" "));
        System.out.println(content.endsWith("!"));
        System.out.println(content.replace(" ", "12"));
        String say = "java is the est programing language";
        String[] arr = say.split(" ");
        System.out.println(arr.length);
        for (String string : arr) {
            System.out.println(string);
        }
        String s1 = "hello";
        String s2 = "wello";
        System.out.println(s1.equals(s2));
        System.out.println(s1.compareTo(s2));

    }
}
