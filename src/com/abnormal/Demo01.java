package com.abnormal;

/**
 * @Description: TODO(异常)
 * @Author lenovo
 * @Date 2021/6/29 16:23
 */
public class Demo01 {

    public static void main(String[] args) {
    /*
    异常：
        1.检查性异常
        2.运行时异常
        3.错误
     捕获异常
     抛出异常
     */

        int a = 1;
        int b = 0;

        try {
            // 监控区域 ***
            System.out.println(a / b);
        } catch (ArithmeticException e) {
            // 捕获异常 ***
            System.out.println("!!!!!!!!!!");
        } finally {
            // 处理善后
            System.out.println("11111");
        }


    }

}
