package com.IO;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @Description: TODO(字节)
 * @Author lenovo
 * @Date 2021/7/22 16:59
 */
public class Demo04 {
    public static void main(String[] args) throws IOException {
        /**
         * 无需手工创建文件
         * 默认数据覆盖管道,路径后面加true变为追加数据管道
         * 1.定位和接通
         * 2.写数据
         */
//        OutputStream os = new FileOutputStream("G:\\code\\java\\java\\javase\\src\\com\\2.txt");
        OutputStream os = new FileOutputStream("javase/src/com/2.txt", true);
        os.write(97);
        // 只能写单字节
        // os.write('高');
//        String s =;
//        byte[] buffer =  "asdasdasdasdadsasda";

        byte[] bytes = new byte[]{98, 95, 97, 97, 97, 25};
        os.write(bytes);
        // 字节数组
        // getBytes(指定编码)
        byte[] bytes1 = "JAVA我不会！".getBytes();
        os.write(bytes1);
        System.out.println(bytes1.length);
        // 换行
        os.write("\r\n".getBytes());
        // 写字节数组的一部分
        byte[] bytes2 = "JAVA我不会！".getBytes();
        os.write(bytes2, 0, 12);

        // 立即刷新数据到文件中去，刷新后管道可以继续使用
        os.flush();

        // 关闭后管道不能使用
        os.close();
    }

}
