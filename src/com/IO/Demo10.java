package com.IO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

/**
 * @Description: TODO(恢复出师表)
 * @Author lenovo
 * @Date 2021/7/26 9:05
 */
public class Demo10 {
    public static void main(String[] args) {
        try (
                BufferedReader br = new BufferedReader(new FileReader("javase/src/com/1.txt"));
                BufferedWriter bw = new BufferedWriter(new FileWriter("javase/src/com/2.txt"));

        ) {
            List<String> datas = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                datas.add(line);
            }
            List<Character> sizes = new ArrayList<>();
            Collections.addAll(sizes, '零', '一', '二', '三', '四');
            Collections.sort(datas, (o1, o2) -> sizes.indexOf(o1.charAt(0)) - sizes.indexOf(o2.charAt(0)));
            for (String str : datas
            ) {
                bw.write(str);
                bw.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
