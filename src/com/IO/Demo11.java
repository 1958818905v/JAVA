package com.IO;

import java.io.*;
import java.nio.charset.Charset;

/**
 * @Description: TODO(字符输入转换InputStreamReader流的使用)
 * @Author lenovo
 * @Date 2021/7/26 9:59
 */
public class Demo11 {
    /**
     * 解决乱码问题:
     * 1.提取GBK的原始字节流
     * 2.通过转换流转换为InputStreamReader
     * 3.包装为缓冲流
     * 4.直接后面加编码
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        // 输入转换流
        File file;
        InputStream is = new FileInputStream("file");
        Charset charset;
        Reader isr = new InputStreamReader(is, "GBk");
        BufferedReader br = new BufferedReader(isr);
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }

        // 输出转换流
        OutputStream os = new FileOutputStream("file");
        // 默认UTF-8
        Writer wos = new OutputStreamWriter(os, "GBK");
        BufferedWriter bw = new BufferedWriter(wos);

    }
}
