package com.IO;

import java.io.*;

/**
 * @Description: TODO(字节流做文件复制)
 * @Author lenovo
 * @Date 2021/7/23 10:14
 */
public class Demo05 {
    /**
     * 复制文件到另一个地方
     * 前后格式一样
     */
    public static void main(String[] args) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream("javase/src/com/2.txt");
            os = new FileOutputStream("javase/src/com/IO/3.txt");
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read()) != -1) {
                os.write(len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 旧方式
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (os != null) {
                try {
                    os.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // 新方式

        }

    }


}
