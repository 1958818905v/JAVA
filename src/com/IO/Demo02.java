package com.IO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * @Description: TODO(字节)
 * @Author lenovo
 * @Date 2021/7/22 14:58
 */
public class Demo02 {
    public static void main(String[] args) throws IOException {
//        File file = new File("G:\\code\\java\\java\\javase\\src\\com\\1.txt");
//        InputStream is = new FileInputStream(file);
        //简化写法

        InputStream is = new FileInputStream("G:\\code\\java\\java\\javase\\src\\com\\1.txt");
        // 设置数组，装桶
//        byte[] buffer = new byte[3];
//        int len=is.read(buffer);
//
//        String rs =new String(buffer,0,len);
//        System.out.println(rs);

        // 循环
        // 定义数组（桶）
        byte[] buffer = new byte[8];
        // 存每次的读取的字节数
        int len;
        while ((len = is.read(buffer)) != -1) {
            String rs = new String(buffer, 0, len);
            System.out.print(rs);
        }
    }
}
