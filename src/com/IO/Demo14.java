package com.IO;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * @Description: TODO(重定向流)
 * @Author lenovo
 * @Date 2021/7/26 11:04
 */
public class Demo14 {
    public static void main(String[] args) throws FileNotFoundException {
        PrintStream ps = new PrintStream("javase/src/com/log.txt");
        System.setOut(ps);
        System.out.println(1);
        System.out.println(2);
        System.out.println(3);
        System.out.println(4);
        System.out.println(5);
        System.out.println(6);

    }

}
