package com.IO;

import java.io.FileReader;
import java.io.IOException;

/**
 * @Description: TODO(字符流 ： 操作文本文件)
 * @Author lenovo
 * @Date 2021/7/23 11:06
 */
public class Demo07 {
    /**
     * @param args
     */
    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("javase/src/com/1.txt");
        // 字符读
//        int code = fr.read();
//        System.out.println((char)code);

//        int code;
//        while((code=fr.read())!=-1){
//            System.out.println((char)code);
//        }

        // 按照字符数组读取内容
        char[] buffer = new char[1024];
//        int len = fr.read(buffer);
//        String s = new String(buffer);
//        System.out.println(s);
        int len;
        while ((len = fr.read(buffer)) != -1) {
            System.out.println(new String(buffer, 0, len));
        }

    }
}
