package com.IO;

import java.io.*;

/**
 * @Description: TODO(字节流做文件复制)
 * @Author lenovo
 * @Date 2021/7/23 10:14
 */
public class Demo06 {
    /**
     * 复制文件到另一个地方
     * 前后格式一样
     * 新特性
     */
    public static void main(String[] args) throws IOException {
        try (
                // 只能定义资源对象
                // 实现了Closeable接口就是资源
                InputStream is = new FileInputStream("javase/src/com/2.txt");
                OutputStream os = new FileOutputStream("javase/src/com/IO/3.txt");
        ) {
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read()) != -1) {
                os.write(len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
