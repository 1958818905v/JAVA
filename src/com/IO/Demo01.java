package com.IO;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/21 15:36
 */
public class Demo01 {
    /**
     * 分类：
     * ·输入流（读取）
     * ·输出流（写入）
     * <p>
     * ·字节流
     * ·inputStream
     * ·FileInputStream
     * ·BufferedInputStream
     * ·OutputStream
     * ·FileOutputStream
     * ·BufferedOutputStream
     * ·字符流(针对文本文件)
     * ·Reader
     * ·FileReader
     * ·BufferedReader
     * ·Writer
     * ·FileReader
     * ·BufferedWriter
     * File只能操作文件本身，不能读取文件对象的内容
     * 读写数据内容，应该使用IO流
     */
    // 不建议使用
    public static void main(String[] args) throws IOException {
        File file = new File("G:\\code\\java\\java\\javase\\src\\com\\1.txt");
        InputStream is = new FileInputStream(file);
        // 读取英文和数字没有问题，读取文字出现问题，效率低
//        System.out.println((char) is.read());
//        System.out.println((char) is.read());
//        System.out.println((char) is.read());
//        System.out.println( is.read());
        // 使用循环
        int a = 0;
        while ((a = is.read()) != -1) {
            System.out.println((char) a);
        }
    }
}
