package com.IO;

import java.io.*;
import java.util.Properties;

/**
 * @Description: TODO(Properties)
 * @Author lenovo
 * @Date 2021/7/26 11:06
 */
public class Demo15 {
    /**
     * 框架的底层技术
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.setProperty("admin1", "18");
        properties.setProperty("admin", "128");
        System.out.println(properties);


        File file;
        OutputStream os = new FileOutputStream("javase/src/com/log.property");
        /**
         * 参数一：管道
         * 参数二：注释
         */
        properties.store(os, "123");

        // 读数据
        properties.load(new FileInputStream("javase/src/com/log.property"));


    }
}
