package com.IO;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * @Description: TODO(打印流)
 * @Author lenovo
 * @Date 2021/7/26 10:54
 */
public class Demo13 {
    /**
     * 1.方便快速的写数据出去
     * 2.实现打印什么出去就是什么
     */
    public static void main(String[] args) throws FileNotFoundException {
        // 1.PrintStream
        /**
         * 直接通向文件
         */
        PrintStream ps = new PrintStream("javase/src/com/1.txt");
        ps.println(98);
        ps.write(98);

        PrintWriter pw = new PrintWriter("javase/src/com/1.txt");
        pw.println(1002);

    }
}
