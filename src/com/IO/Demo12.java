package com.IO;

import java.io.*;

/**
 * @Description: TODO(序列化)
 * @Author lenovo
 * @Date 2021/7/26 10:31
 */
public class Demo12 {
    /**
     * 对象序列化：把对象直接存在文件中  对象-->文件 objectInputStream
     * ·对象必须实现序列化接口(Serializable)
     * 对象反序列化：把文件直接存在对象中  文件-->对象 objectOutputStream
     */

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        User user = new User("Sun", 18, "123456");
        File file;
        ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("javase/src/com/obj.dat")));
        oos.writeObject(user);
        oos.close();

        // transient 表示不参与序列化
        ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream("javase/src/com/obj.dat")));
        User user1 = (User) ois.readObject();
        System.out.println(user1);
        ois.close();

    }
}
