package com.IO;

import java.io.*;

/**
 * @Description: TODO(IO流子类 （ 缓冲流 ）)
 * @Author lenovo
 * @Date 2021/7/23 11:27
 */
public class Demo09 {
    /**
     * 1.BufferedInputStream ：字节缓冲输入流
     * ·性能好
     * ·高级类
     * ·多了缓存池
     * ·原理：缓冲字节输入流管道自带了一个缓冲池（8kb），每次可以直接借用操作系统的功能最多提取8kb的数据到缓冲池中，直接从缓存池读数据
     * 2.BufferedOutStream:字节缓冲输出流
     * ·提高写数据的性能
     * 3.BufferedReader：字符缓冲输入流
     * ·readLine():读取一行数据返回，读取完毕返回NULL
     * ··性能优化
     * ··业务需要
     * 4.BufferWriter：字节缓冲输出流
     * ·newLine():换行功能，新建一行
     * 目的：提高效率
     */

    public static void main(String[] args) throws IOException {
        InputStream bis = new BufferedInputStream(new FileInputStream("javase/src/com/1.txt"));
        byte[] bytes = new byte[3];
        int len;
        while ((len = bis.read(bytes)) != -1) {
            System.out.println(new String(bytes, 0, len));
        }
        bis.close();


        OutputStream bos = new BufferedOutputStream(new FileOutputStream("javase/src/com/1.txt", true));
        bos.write('a');
        bos.write(100);
        bos.close();

        BufferedWriter bw = new BufferedWriter(new FileWriter("javase/src/com/5.txt", true));
        bw.write('a');
        bw.newLine();
        bw.write(100);
        bw.close();


        BufferedReader br = new BufferedReader(new FileReader("javase/src/com/1.txt"));
//       char[] buffer = new char[1024];
//       int len1;
//       while ((len= br.read(buffer))!=-1){
//           System.out.println(new String(buffer,0,len1));
//       }
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }

        br.close();
    }


}
