package com.Multithreading;

/**
 * @Description: TODO(死锁)
 * @Author lenovo
 * @Date 2021/7/29 16:51
 */
public class Lock01 {
    /**
     * 1.多个线程各自占有一些共享资源，并且互相等待其他线程占有的资源才能运行，而导致俩个或者多个线程都在等待对方释放资源，都停止执行的情况。
     * 某一个同步块同时拥有“俩个以上对象的锁”时，就可能会发生死锁的问题。
     * 锁里不能套锁（否则就把对方锁死）
     * 产生的四个必要条件：
     * 1.互斥条件：一个资源每次只能被一个进程使用
     * 2.请求与保持条件：一个进程因请求资源而阻塞时，对方获得的资源保持不放
     * 3.不剥夺条件：进程以获得的资源，在未使用之前，不能强行剥夺
     * 4.循环等待条件：若干进程之间形成一种头尾相接的循环等资源关系
     * 只要破解其一锁就无法形成
     */
    public static void main(String[] args) {
        Makeup m1 = new Makeup(0, "灰姑娘");
        Makeup m2 = new Makeup(1, "白雪公主");

        m1.start();
        m2.start();
    }

}

class Lipstick {

}

class Mirror {

}

class Makeup extends Thread {
    static Lipstick lipstick = new Lipstick();
    static Mirror mirror = new Mirror();

    int chose;
    String name;

    Makeup(int chose, String name) {
        this.chose = chose;
        this.name = name;
    }


    @Override
    public void run() {
        makeup();
    }


    private void makeup() {
        if (chose == 0) {
            synchronized (lipstick) {
                System.out.println(this.name + "获得口红的锁");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            synchronized (mirror) {
                System.out.println(this.name + "获得镜子的锁");
            }
        } else {
            synchronized (mirror) {
                System.out.println(this.name + "获得镜子的锁");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            synchronized (lipstick) {
                System.out.println(this.name + "获得口红的锁");
            }
        }
    }

}