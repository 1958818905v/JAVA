package com.Multithreading;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/28 15:29
 */
public class Threadjoin implements Runnable {
    public static void main(String[] args) throws InterruptedException {
        Threadjoin threadjoin = new Threadjoin();
        Thread thread = new Thread(threadjoin);
        thread.start();


        for (int i = 0; i < 500; i++) {
            if (i == 200) {
                thread.join();
            }
            System.out.println("main" + i);
        }
    }

    /**
     * Join合并线程，待此线程执行完成后，在执行其他线程，造成其他线程阻塞
     */
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(i);
        }
    }
}
