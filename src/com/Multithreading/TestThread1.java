package com.Multithreading;

/**
 * @Description: TODO(线程的创建)
 * @Author lenovo
 * @Date 2021/7/26 11:42
 */
public class TestThread1 extends Thread {
    //主线程
    public static void main(String[] args) {

        //创建线程对象
        TestThread1 testThread1 = new TestThread1();
        //start（） 方法用来 启动线程；
        testThread1.start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("有人在在睡觉sleep-->" + i);
        }

    }

    /**
     * 1.thread(class)-->继承Thread类（重点）
     * ·自定义线程的类
     * ·重写Run方法
     * ·创捷线程对象，调用start（）方法启动线程
     * ·总结：
     * 1.线程开启不一定立即执行，由CUP调度
     * 2.Runnable(接口)-->实现Runnable（重点）
     * ·实现Runnable接口，重写run方法，执行线程需要丢入runnable接口的实现类，调用start（）
     * ·区别：
     * 1.子类继承Thread类具备多线程能力；实现Runnable具有多线程能力
     * 2.启动线程：Thread（子类对象名。start（））；Runnable（传入目标对象+Thread对象。start（））
     * 3.不建议使用Thread（避免OOP单继承局限性）；建议使用Runnable（避免单继承局限性，灵活方便，方便同一个对象被多个线程使用）
     * 3.Callable(接口)-->实现Callable（了解）
     */

    //自定义run方法的线程
    @Override
    public void run() {
        //线程执行体
        for (int i = 0; i < 200; i++) {
            System.out.println("有人在写笔记-->" + i);
        }
    }


}




