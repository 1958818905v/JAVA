package com.Multithreading;

/**
 * @Description: TODO(Runnable)
 * @Author lenovo
 * @Date 2021/7/27 9:47
 */

// 推荐使用
public class TestThread2 implements Runnable {

    public static void main(String[] args) {
        // 创建Runnable接口的实现类对象
        TestThread2 testThread2 = new TestThread2();
        // 创建线程对象。通过线程对象来开启线程（代理）
        // Thread t = new Thread(testThread2);
        // t.start();

        new Thread(testThread2).start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("多线程" + i);
        }
    }

    @Override
    public void run() {
        for (int i = 0; i < 200; i++) {
            System.out.println("我在看代码-----" + i);
        }
    }


}
