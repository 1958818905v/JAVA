package com.Multithreading;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @Description: TODO(练习Thread ， 实现多线程下载图片)
 * @Author lenovo
 * @Date 2021/7/27 9:25
 */
//public class Test01 extends Thread {
public class Test01 implements Runnable {

    private String url;
    private String name;

    public Test01(String url, String name) {
        this.name = name;
        this.url = url;
    }

    public static void main(String[] args) {
        Test01 test01 = new Test01("https://pic1.zhimg.com/80/v2-99cfb57536808fbd1551e0da577825ef_720w.jpg", "孩子1.jpg");
        Test01 test02 = new Test01("https://pic1.zhimg.com/80/v2-99cfb57536808fbd1551e0da577825ef_720w.jpg", "孩子2.jpg");
        Test01 test03 = new Test01("https://pic1.zhimg.com/80/v2-99cfb57536808fbd1551e0da577825ef_720w.jpg", "孩子3.jpg");

        new Thread(test01).start();
        new Thread(test02).start();
        new Thread(test03).start();
//        // 线程同时执行
//        test01.start();
//        test02.start();
//        test03.start();

    }

    @Override
    public void run() {
        WebDownLoader webDownLoader = new WebDownLoader();
        webDownLoader.downLoader(url, name);
        System.out.println(name + "下载了文件");
    }
}


// 下载器
class WebDownLoader {
    // 下载方法
    public void downLoader(String url, String name) {
        try {
            FileUtils.copyURLToFile(new URL(url), new File(name));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO异常");
        }
    }
}