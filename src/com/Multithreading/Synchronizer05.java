package com.Multithreading;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: TODO(同步方法及同步块)
 * @Author lenovo
 * @Date 2021/7/29 10:56
 */
public class Synchronizer05 {
    /**
     * 线程不安全
     */
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < 10000; i++) {
            new Thread(() -> {
                list.add(Thread.currentThread().getName());
            }).start();

        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(list.size());
    }
}
  


