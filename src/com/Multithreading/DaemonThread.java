package com.Multithreading;

/**
 * @Description: TODO(守护线程)
 * @Author lenovo
 * @Date 2021/7/29 9:31
 */
public class DaemonThread {
    /**
     * 线程分为用户线程和守护线程
     * 虚拟机必须确保用户线程执行
     * 虚拟机不用等待守护线程执行完毕
     * 如，后台记录操作日志，监控内存，垃圾回收等
     */
    public static void main(String[] args) {
        God god = new God();
        You you = new You();


        Runnable target;
        Thread t = new Thread(god);
        // 默认是false表示是用户线程，正常的线程都是用户线程
        t.setDaemon(true);
        t.start();

        new Thread(you).start();

    }


    static class You implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i < 365000; i++) {
                System.out.println("你！");
            }
            System.out.println("good");
        }
    }


    static class God implements Runnable {
        @Override
        public void run() {
            System.out.println("上帝！");
        }
    }
}
