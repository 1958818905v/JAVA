package com.Multithreading;

/**
 * @Description: TODO(线程礼让)
 * @Author lenovo
 * @Date 2021/7/28 9:34
 */
public class ThreadPolite {
    /**
     * 1.线程礼让，让当前正在执行的线程暂停，但不阻塞
     * 2.将线程从运行状态转为就绪状态
     * 3.让CPU重新调度，礼让不一定成功看CPU心情
     */
    public static void main(String[] args) {
        MyYield myYield = new MyYield();
        new Thread(myYield, "a").start();
        new Thread(myYield, "b").start();
    }


}

class MyYield implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "开始执行");
        // 礼让
        Thread.yield();
        System.out.println("线程停止执行");
    }
}