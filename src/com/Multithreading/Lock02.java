package com.Multithreading;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Description: TODO(Lock锁)
 * @Author lenovo
 * @Date 2021/7/29 17:35
 */
public class Lock02 {
    /**
     * 1.线程同步机制--通过显示定义同步锁对象来实现，同步锁使用Lock对象来充当
     * 2.Java.util.locks.Lock接口是控制多个线程对共享资源进行访问的工具。
     * 锁提供了对共享资源的独占访问，每次只能有一个线程对Lock对象加锁，线程开始访问共享资源之前应先获得Lock对象
     * 3.ReentrantLock类实现了Lock，他拥有与synchronize相同的并发性和内存语义，在实现线程安全的控制中，
     * 比较常用的是ReentrantLock（可重复锁），可以显式加锁、释放锁
     * <p>
     * 加锁卸载try{}里，有异常解锁卸载finally里
     * <p>
     * synchronize与Lock的对比
     * 1.Lock是显式锁（手动开关锁），synchronize是隐式锁（出了作用域自动释放）
     * 2.Lock只有代码块锁，synchronize有代码块锁和方法锁
     * 3.使用Lock锁，JVM将花费较少的时间来调度线程，性能更好。并且具有更好的扩展性（子类）
     * 4.优先使用顺序：
     * Lock>代码同步块（已经进入了方法体，分配了相应资源）>同步方法（在方法体之外）
     */
    public static void main(String[] args) {
        Lock l1 = new Lock();
        new Thread(l1).start();
        new Thread(l1).start();
        new Thread(l1).start();
    }

}

class Lock implements Runnable {
    private final ReentrantLock lock = new ReentrantLock();
    int ticketNum = 10;

    @Override
    public void run() {
        while (true) {
            // 加锁
            try {
                lock.lock();
                if (ticketNum > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(ticketNum--);
                } else {
                    break;
                }
            } finally {
                // 解锁
                lock.unlock();
            }


        }
    }
}
