package com.Multithreading;

interface Marry {
    void happyMarry();
}

/**
 * @Description: TODO(静态代理)
 * @Author lenovo
 * @Date 2021/7/27 14:45
 */
public class acting {
    /**
     * 静态代理模式总结：
     * 1.真实对象和代理对象都要实现同一个接口
     * 2.代理对象要代理真实的角色
     * 好处（优点 ）：
     * 1.代理对象可以做很多真实对象做不了的事情
     * 2.真实对象专注做自己的事情
     */
    public static void main(String[] args) {
        You you = new You();
        // 代理
        new Thread(() -> System.out.println("123")).start();

        new weddingCompany(you).happyMarry();
//        weddingCompany weddingCompany = new weddingCompany(you);
//        weddingCompany.happyMarry();
    }
}

// 真实角色
class You implements Marry {

    @Override
    public void happyMarry() {
        System.out.println("!!!!");
    }
}


// 代理角色
class weddingCompany implements Marry {
    // 代理谁-->真实目标角色
    private Marry target;

    public weddingCompany(Marry target) {
        this.target = target;
    }

    @Override
    public void happyMarry() {
        before();
        // 真实对象
        this.target.happyMarry();
        after();
    }

    private void before() {
        System.out.println("布置现场！");
    }

    private void after() {
        System.out.println("收尾款");
    }


}