package com.Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description: TODO(线程池)
 * @Author lenovo
 * @Date 2021/7/30 10:07
 */
public class ThreadPool {
    /**
     * 背景：经常创建和销毁、使用量特别大的资源，比如并发情况下的线程，对性能影响很大
     * 思路：提前创建好多个线程，放入线程池中，使用时直接获取，使用万放回池中，避免重复创建销毁，实现复用
     * 好处：
     *  1.提高响应速度（减少创建新线程的时间）
     *  2.降低资源消耗（重复利用线程池中线程，不需要每次都创建）
     *  3.便于线程管理
     *      1.corePoolSize：核心池大小
     *      2.maximumPoolSize：最大线程数
     *      3.keepAliveTime：线程没有任务是最多保持多长时间后会终止
     *
     *  Api相关：ExecutorService和Executors
     */


    public static void main(String[] args) {
        // 创建线程池(池子大小)
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.execute(new MyThread());
        executorService.execute(new MyThread());
        executorService.execute(new MyThread());
        executorService.execute(new MyThread());

        executorService.shutdown();
    }
}

class MyThread implements Runnable{
    @Override
    public void run() {
            System.out.println(Thread.currentThread().getName());

    }
}
