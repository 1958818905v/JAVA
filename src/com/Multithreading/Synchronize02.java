package com.Multithreading;

/**
 * @Description: TODO(三个不安全的案例)
 * @Author lenovo
 * @Date 2021/7/29 10:21
 */
public class Synchronize02 {
    /**
     * 不安全（不同步）
     *
     * @param args
     */
    public static void main(String[] args) {
        Buy buy = new Buy();

        new Thread(buy, "张三").start();
        new Thread(buy, "李四").start();
        new Thread(buy, "王五").start();

    }
}

class Buy implements Runnable {
    int ticketNum = 10;
    boolean flag = true;

    @Override
    public void run() {
        while (flag) {
            BuyTicket();
        }

    }

    public synchronized void BuyTicket() {
        if (ticketNum <= 0) {
            flag = false;
            return;
        }
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "拿到第" + ticketNum-- + "票");
    }

}



