package com.Multithreading;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Description: TODO(Juc安全类型的集合)
 * @Author lenovo
 * @Date 2021/7/29 16:47
 */
public class Juc {
    public static void main(String[] args) {
        // juc线程安全的集合
        CopyOnWriteArrayList<String> writeArrayList = new CopyOnWriteArrayList<String>();
        for (int i = 0; i < 10000; i++) {
            new Thread(() -> {
                writeArrayList.add(Thread.currentThread().getName());
            }).start();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(writeArrayList.size());
    }
}
