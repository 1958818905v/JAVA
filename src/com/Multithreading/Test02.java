package com.Multithreading;

/**
 * @Description: TODO(龟兔赛跑)
 * @Author lenovo
 * @Date 2021/7/27 11:12
 */
public class Test02 implements Runnable {
    /**
     * 1.首先来个赛道距离，然后要离距离越来越近
     * 2.判断比赛是否结束
     * 3.打印出胜利者
     * 4.龟兔赛跑开始
     * 5.故事中是乌龟嬴的，兔子需要睡觉，所以我们要来模拟兔子睡觉
     * 6.终于，乌龟赢得比赛
     */

    private static String Winner;

    public static void main(String[] args) {
        Test02 test02 = new Test02();
        new Thread(test02, "乌龟").start();
        new Thread(test02, "兔子").start();
    }

    @Override
    public void run() {
        for (int i = 0; i <= 100000; i++) {

            // 模拟兔子休息
            if (Thread.currentThread().getName().equals("兔子") && i % 10 == 0) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            // 判断比赛是否结束
            boolean flag = gameOver(i);
            // 比赛结束停止程序
            if (flag) {
                break;
            }

            System.out.println(Thread.currentThread().getName() + "--》跑了第" + i + "步");
        }
    }

    // 判断是否完成比赛
    public boolean gameOver(int steps) {
        // 判断是否有胜利者
        if (Winner != null) {
            return true;
        } else {
            if (steps >= 100000) {
                Winner = Thread.currentThread().getName();
                System.out.println("winner is a :" + Winner);
                return true;
            }
        }
        return false;
    }


}
