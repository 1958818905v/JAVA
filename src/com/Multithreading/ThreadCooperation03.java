package com.Multithreading;

/**
 * @Description: TODO(信号灯法)
 * @Author lenovo
 * @Date 2021/7/30 9:46
 */
public class ThreadCooperation03 {
    /**
     * 通过标志位解决
     */
    public static void main(String[] args) {
        TV tv = new TV();

        new actor(tv).start();
        new audience(tv).start();

    }
}

class actor extends Thread {
    TV tv;

    public actor(TV tv) {
        this.tv = tv;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            if (i % 2 == 0) {
                this.tv.play("节目");
            } else {
                this.tv.play("广告");
            }
        }
    }
}

class audience extends Thread {
    TV tv;

    public audience(TV tv) {
        this.tv = tv;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            tv.watch();
        }
    }
}

class TV {
    String voice;
    boolean flag = true;

    public synchronized void play(String voice) {
        if (!flag) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("表演了：" + voice);
        this.notifyAll();
        this.voice = voice;
        this.flag = !this.flag;
    }

    public synchronized void watch() {
        if (flag) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("观看了" + voice);
        this.notifyAll();
        this.flag = !this.flag;
    }

}

