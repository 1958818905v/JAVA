package com.Multithreading;

/**
 * @Description: TODO(线程协作)
 * @Author lenovo
 * @Date 2021/7/30 8:59
 */
public class ThreadCooperation01 {
    /**
     * 生产者消费者模式（问题）
     * 方法：
     *  wait（）：表示线程一直等待，直到其他线程通知，与sleep不同，会释放锁
     *  wait（long time）：指定等待毫秒数
     *  notify（）：唤醒一个等待的线程
     *  notifyAll（）：唤醒同一个对象上所有调用wait（）方法的线程，优先级别高的线程优先调度
     *
     *  均是Object类，都只能在同步方法或者同步代码中使用，否则会抛出异常IIIegalMonitorStateExption
     *
     *  解决方式：
     *      1.并发协作模型“生产者、消费者模式”--->管程法
     *          生产者：负责生产数据的模块（可能是方法、对象、线程、进程）
     *          消费者：负责处理数据的模块（可能是方法、对象、线程、进程）
     *          缓冲区：消费者不能在建瓯空额使用生产者的数据，他们之间有个“缓冲区”
     *          生产者将生产好的数据放入缓冲区，消费者从缓冲区拿出数据
     *      2.信号灯法：
     *          1.通过标记位判断
     */
}
