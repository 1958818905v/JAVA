package com.Multithreading;

// 1.定义一个函数式接口
interface Ilike {
    void Lambda();
}

/**
 * @Description: TODO(Lamda表达式)
 * @Author lenovo
 * @Date 2021/7/27 16:14
 */
public class Lamda {
    public static void main(String[] args) {

        Ilike like = new Like();
        like.Lambda();

        like = new Like2();
        like.Lambda();


        // 4.局部内部类
        class Like3 implements Ilike {
            @Override
            public void Lambda() {
                System.out.println("I Like lambda3!");
            }
        }

        like = new Like3();
        like.Lambda();

        // 5. 匿名内部类,没有类的名称，必须借助接口或父类
        like = new Like() {
            @Override
            public void Lambda() {
                System.out.println("I Like lambda4!");
            }
        };
        like.Lambda();

        // 6.用Lambda简化
        like = () -> {
            System.out.println("I Like lambda5!");
        };
        like.Lambda();

    }

    /**
     * 希腊字母表中排序第十一位的字母
     * 作用：
     * 1.避免匿名内部类定义过多
     * 2.其实质属于函数式编程的概念
     * ·（params）-> expression[表达式]
     * ·（params）-> statement[语句]
     * ·（params）-> { statements }
     * 为什么要使用：
     * 1.避免匿名内部类定义过多
     * 2.简化代码
     * 3.只留下核心的代码
     * 函数式接口：
     * 1.任何函数，如果质保函唯一一个抽象方法，那么他就是一个函数式接口
     * 2.对于函数式接口，我们可以通过Lambda表达式来创建该接口对象
     */

    // 3.静态内部类
    static class Like2 implements Ilike {
        @Override
        public void Lambda() {
            System.out.println("I Like lambda2!");
        }
    }

}

// 2.实现类
class Like implements Ilike {
    @Override
    public void Lambda() {
        System.out.println("I Like lambda!");
    }
}
