package com.Multithreading;

/**
 * @Description: TODO(观测线程状态)
 * @Author lenovo
 * @Date 2021/7/28 15:35
 */
public class ThreadDemo08 {
    /**
     * Thread.State
     * ·new：尚未启动的线程处于此状态
     * ·RUNNABLE：在JAVA虚拟机中执行的线程出于此状态
     * ·BLOCKED：被阻塞等待监视器锁定的线程处于此状态
     * ·WAITING：正在等待另一个线程执行特定动作的线程处于此状态
     * ·TIMED_WAITING：正在等待另一个线程执行动作达到指定等待时间的线程处于此状态
     * ·TERMINATED：以退出的线程处于此状态
     * 死亡之后的线程不能在此启动
     */

// 观察测试线程状态
    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("/////////");
        });

        // 观察状态
        Thread.State state = thread.getState();
        System.out.println(state);


        // 观察启动后
        thread.start();
        state = thread.getState();
        System.out.println(state);

        while (state != Thread.State.TERMINATED) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            state = thread.getState();
            System.out.println(state);
        }
    }
}
