package com.Multithreading;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.*;

/**
 * @Description: TODO(Callable)
 * @Author lenovo
 * @Date 2021/7/27 14:28
 */
public class CallableDemo01 implements Callable<Boolean> {
    /**
     * 1.实现Callable接口，返回值类型
     * 2.重写call方法，需要抛出异常
     * 3.创建目标对象
     * 4.创建执行服务：ExecutorService ser = Executors.newFixedThreadPool（1）;
     * 5.提交执行：Future<Boolean> result1 = ser.submit(t1);
     * 6.获取服务：boolean r1 = result1.get();
     * 7.关闭服务：ser.shutdownNow();
     */


    private String url;
    private String name;

    public CallableDemo01(String url, String name) {
        this.name = name;
        this.url = url;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CallableDemo01 test01 = new CallableDemo01("https://pic1.zhimg.com/80/v2-99cfb57536808fbd1551e0da577825ef_720w.jpg", "孩子1.jpg");
        CallableDemo01 test02 = new CallableDemo01("https://pic1.zhimg.com/80/v2-99cfb57536808fbd1551e0da577825ef_720w.jpg", "孩子2.jpg");
        CallableDemo01 test03 = new CallableDemo01("https://pic1.zhimg.com/80/v2-99cfb57536808fbd1551e0da577825ef_720w.jpg", "孩子3.jpg");

        ExecutorService ser = Executors.newFixedThreadPool(3);

        Callable<Boolean> t1 = null;
        Callable<Boolean> t2 = null;
        Callable<Boolean> t3 = null;
        Future<Boolean> r1 = ser.submit(t1);
        Future<Boolean> r2 = ser.submit(t2);
        Future<Boolean> r3 = ser.submit(t3);

        boolean rs1 = r1.get();
        boolean rs2 = r1.get();
        boolean rs3 = r1.get();

        ser.shutdown();
    }

    @Override
    public Boolean call() {
        WebDownLoader1 webDownLoader = new WebDownLoader1();
        webDownLoader.downLoader(url, name);
        System.out.println(name + "下载了文件");
        return true;
    }


}

class WebDownLoader1 {
    // 下载方法
    public void downLoader(String url, String name) {
        try {
            FileUtils.copyURLToFile(new URL(url), new File(name));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO异常");
        }
    }
}

