package com.Multithreading;

/**
 * @Description: TODO(同步方法及同步块)
 * @Author lenovo
 * @Date 2021/7/29 10:56
 */
public class Synchronizer06 {
    /**
     * 分类：synchronize方法和synchronized块
     *  ·synchronize方法控制对象的访问，每个对象都对应一把锁，每个synchronize方法都必须获得该方法的对象的锁才能执行，否则线程会阻塞，方法一旦执行，就独占该锁，直到方法返回才释放该锁，后面被阻塞的线程才能获得这个锁，继续执行
     *  ·缺陷：若将一个大的方法申明为synchronize将会影响效率
     *  ·同步块
     *      1.synchronize（Obj）{}
     *      2.obj称为同步监视器
     *          1.obj可以是任何对象，但是推荐使用共享资源作为同步监视器
     *          2.同步方法中无需指定同步监视器，因为同步方法的同步监视器就是this，就是这个对象本身，或者是class【反射中讲解】
     *      3.执行过程
     *          1.第一个线程访问，锁定同步监视器，执行其中代码
     *          2.第二个线程访问，发现同步监视器被锁定，无法访问
     *          3.第一个线程访问完毕，解锁同步监视器
     *          4.第二个线程访问，发现同步监视器没有锁，然后锁定并访问
     * 同步方法：public synchronize void method(int args){}
     */

}
  


