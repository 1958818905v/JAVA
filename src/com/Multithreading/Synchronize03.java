package com.Multithreading;


public class Synchronize03 {
    public static void main(String[] args) {
        Account account = new Account(1000, "基金");
        Drawing you = new Drawing(account, 50, "花花");
        Drawing girl = new Drawing(account, 100, "小花");
        you.start();
        girl.start();
    }
}

// 账户
class Account {
    int money;
    String name;

    public Account(int money, String name) {
        this.money = money;
        this.name = name;
    }
}

// 银行
class Drawing extends Thread {
    Account account;
    int drawingMoney;
    int nowMoney;

    public Drawing(Account account, int drawingMoney, String name) {
        super(name);
        this.account = account;
        this.drawingMoney = drawingMoney;
    }


    @Override
    public void run() {
        // 锁的对象为变化的量
        synchronized (account) {
            // 判断钱
            if (account.money - drawingMoney < 0) {
                System.out.println(Thread.currentThread().getName() + "余额不足！");
                return;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//        余额
            account.money = account.money - drawingMoney;
//        手里的钱
            nowMoney = nowMoney + drawingMoney;
            System.out.println("余额为：" + account.money);
            System.out.println(this.getName() + "手里有：" + nowMoney);
        }


    }
}