package com.Multithreading;

interface Ilove {
    void love(int a);
}

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author lenovo
 * @Date 2021/7/27 16:42
 */
public class Lambda2 {
    /**
     * 总结：
     * 1.Lambda表达式只能有一行代码的情况下才能简化，如多行就代码块包裹
     * 2.必须是函数式接口
     * 3.多个参数也可以去掉参数类型，要去都去，必须加括号
     */
    public static void main(String[] args) {
        Ilove love = null;
        // 1.Lambda 简化
        love = (int a) -> {
            System.out.println("I Love YOU!!" + a);
        };
        // 简化参数类型
        love = (a) -> {
            System.out.println("I Love YOU!!" + a);
        };

        // 简化括号
        love = a -> {
            System.out.println("I Love YOU!!" + a);
        };

        // 简化花括号
        love = a ->
                System.out.println("I Love YOU!!" + a);


        love.love(12);
    }
}



