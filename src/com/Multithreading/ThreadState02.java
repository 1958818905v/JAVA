package com.Multithreading;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description: TODO(线程休眠)
 * @Author lenovo
 * @Date 2021/7/28 9:07
 */
public class ThreadState02 {
    /**
     * 1.sleep（时间）指定当前线程阻塞的毫秒数；
     * 2.sleep存在异常InterruptedException
     * 3.sleep时间达到后线程进入就绪状态
     * 4.sleep可以模拟网络延时，倒计时等
     * 每个对象都有一个锁，sleep不会释放锁
     */

    // 模拟网络延时：放大问题的发生性
    // 模拟倒计时
    public static void tenDown() throws InterruptedException {
        int num = 10;
        while (true) {
            Thread.sleep(1000);
            System.out.println(num--);
            if (num <= 0) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        // 模拟倒计时
//        try {
//            tenDown();
//        } catch (InterruptedException e) {
//            e.printStackTrace();

        // 打印当前时间
        Date startTime = new Date(System.currentTimeMillis());
        while (true) {
            try {
                Thread.sleep(1000);
                System.out.println(new SimpleDateFormat("HH:mm;ss").format(startTime));
                // 更新当前时间
                startTime = new Date(System.currentTimeMillis());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
