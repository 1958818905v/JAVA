package com.Multithreading;

/**
 * @Description: TODO(管程法)
 * @Author lenovo
 * @Date 2021/7/30 9:17
 */
public class ThreadCooperation02 {
    /**
     * 利用缓冲区解决
     */


    public static void main(String[] args) {
        SynContainer synContainer = new SynContainer();
        new Producter(synContainer).start();
        new Consumer(synContainer).start();
    }
}


// 生产者
class Producter extends Thread {
    SynContainer container;

    public Producter(SynContainer container) {
        this.container = container;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            container.push(new Chicken(i));
            System.out.println("生产了第" + i + "只鸡");
        }
    }
}


// 消费者
class Consumer extends Thread {
    SynContainer container;

    public Consumer(SynContainer container) {
        this.container = container;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("消费了第" + container.pop().id + "只鸡");
        }
    }
}

// 产品
class Chicken {
    int id;

    public Chicken(int id) {
        this.id = id;
    }
}

// 缓冲区
class SynContainer {
    // 容器大小
    Chicken[] chickens = new Chicken[10];
    int count = 0;

    // 生产者生产产品
    public synchronized void push(Chicken chicken) {
        if (count == chickens.length) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        chickens[count] = chicken;
        count++;

        this.notifyAll();
    }

    // 消费者消费产品
    public synchronized Chicken pop() {
        if (count == 0) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        count--;
        Chicken chicken = chickens[count];


        this.notifyAll();
        return chicken;
    }

}
