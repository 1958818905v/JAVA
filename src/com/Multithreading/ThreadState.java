package com.Multithreading;

/**
 * @Description: TODO(线程状态)
 * @Author lenovo
 * @Date 2021/7/27 17:08
 */
public class ThreadState implements Runnable {

/**
 * 1. 五大状态
 *  新生状态-->就绪状态-->运行状态-->阻塞状态（sleep、wait或同步锁定时）-->死亡状态
 *  2.方法
 *      1.setPriority（int newPriority）：更改线程优先级
 *      2.sleep（）：在指定毫秒数内让当前正在执行的线程休眠
 *      3.join（）：等待该线程终止
 *      4.yield（）：暂停当前赈灾执行的线程对象，并执行其他线程
 *      5.interrupt()：中断线程（不要使用）
 *          ·通过标志位停止线程
 *      6.isAlive（）：测试线程是否处于活动状态
 */


    /**
     * 测试Stop
     * 1.建议线程正常停止-->利用次数，不建议使用死循环
     * 2.建议使用标志位-->设置一个标志位
     * 3.不要使用stop或者destrop等过时或者JDK不建议使用的方法
     */

    /**
     * 1.设置一个标记位
     */
    private boolean flag = true;

    public static void main(String[] args) {
        ThreadState threadState = new ThreadState();
        new Thread(threadState).start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("main" + i);
            if (i == 900) {
                // 调用Stop方法切换标记位
                threadState.stop();
                System.out.println("线程停止");
            }
        }


    }
    //2.设置一个公开的方法停止线程，转换标记位

    @Override
    public void run() {
        int i = 0;
        while (flag) {
            System.out.println(i++);
        }
    }

    public void stop() {
        this.flag = false;
    }


}
