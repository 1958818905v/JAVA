package com.Multithreading;

/**
 * @Description: TODO(多线程概述)
 * @Author lenovo
 * @Date 2021/7/26 11:21
 */
public class Demo01 {
    /**
     *简介：
     *  ·进程（Process）：是系统资源分配的单位
     *  ·线程（Thread）：线程是CUP调度和执行的单位
     *
     * 核心：
     *  1.线程就是独立的执行路径
     *  2.在程序运行时，即使没有自己创建线程，后台也会有多个线程，如主线程，GC
     *  3.main（）称为主线程，为系统的入口，用于执行整个程序
     *  4.在一个进程中，如果开辟了多个线程，线程的运行由调度器安排调度，调度器是与操作系统紧密相关的，先后顺序是不能干预的
     *  5.对同一份资源造作时，会存在抢资源的问题，需要加入并发控制
     *  6.线程会带来额外的开销，如CUP调度时间，并发控制开销
     *  7.每个线程在自己的工作内存交互，内存控制不当会造成数据不一致
     */

}
