package com.Multithreading;

/**
 * @Description: TODO(线程优先级)
 * @Author lenovo
 * @Date 2021/7/29 9:16
 */
public class ThreadPriority {
    /**
     * 1.Java提供一个线程调度器来监控程序中启动后进入就绪状态的所有线程，线程调度器按照优先级决定应该调度那个线程来执行。
     * 2.优先级用数字表示，范围1-10
     * Thread.MIN_PRIORITY = 1;
     * Thread.MAX_PRIORITY = 10;
     * Thread.NORM_PRIORITY = 5;
     * 3.使用以下方式改变或获取优先级
     * getPriority().setPriority(int xxx);
     * <p>
     * 性能倒置
     */
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName() + "--" + Thread.currentThread().getPriority());
        MyPriority myPriority = new MyPriority();
        Thread t1 = new Thread(myPriority);
        Thread t2 = new Thread(myPriority);
        Thread t3 = new Thread(myPriority);
        Thread t4 = new Thread(myPriority);
        Thread t5 = new Thread(myPriority);
        Thread t6 = new Thread(myPriority);

        // 先设置优先级在启动
        t1.start();

        t2.setPriority(1);
        t2.start();


        t3.setPriority(4);
        t3.start();


        t4.setPriority(Thread.MAX_PRIORITY);
        t4.start();


        t5.setPriority(8);
        t5.start();


        t6.setPriority(3);
        t6.start();
    }

    static class MyPriority implements Runnable {
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + "--" + Thread.currentThread().getPriority());
        }
    }

}
